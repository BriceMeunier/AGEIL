from django.apps import AppConfig


class EnseignementmanagerConfig(AppConfig):
    name = 'enseignementManager'
