# coding: utf8
from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'creerTreeViewModeleFormation$', views.creerTreeViewModeleFormation),
	url(r'modifierTreeViewModeleFormation/(?P<idModele>\d+)$', views.modifierTreeViewModeleFormation),
	url(r'supprimerModeleFormation/(?P<idModele>\d+)$', views.supprimerModeleFormation),
	url(r'paramModifierInformationsModeleFormation/(?P<idModele>\d+)$', views.paramModifierInformationsModeleFormation),
	url(r'modifierInformationsModeleFormation$', views.modifierInformationsModeleFormation),
	
	url(r'copierModeleFormation/(?P<idModele>\d+)$', views.copierModeleFormation),
	
	url(r'paramModifierSemestre/(?P<idModele>\d+)/(?P<idSemestre>\d+)$', views.paramModifierSemestre),
	url(r'modifierSemestre$', views.modifierSemestre),
	
	url(r'paramAjouterSemestre/(?P<idModele>\d+)$', views.paramAjouterSemestre),
	url(r'ajouterSemestre$', views.ajouterSemestre),
	
	url(r'supprimerSemestre/(?P<idModele>\d+)/(?P<idSemestre>\d+)$', views.supprimerSemestre),
	
	url(r'paramModifierUE/(?P<idModele>\d+)/(?P<idUE>\d+)$', views.paramModifierUE),
	url(r'modifierUE$', views.modifierUE),
	
	url(r'paramAjouterUE/(?P<idModele>\d+)/(?P<idSemestre>\d+)$', views.paramAjouterUE),
	url(r'ajouterUE$', views.ajouterUE),
	
	url(r'supprimerUE/(?P<idModele>\d+)/(?P<idUE>\d+)$', views.supprimerUE),
	
	url(r'paramModifierModule/(?P<idModele>\d+)/(?P<idModule>\d+)$', views.paramModifierModule),
	url(r'modifierModule$', views.modifierModule),
	
	url(r'paramAjouterModule/(?P<idModele>\d+)/(?P<idUE>\d+)$', views.paramAjouterModule),
	url(r'ajouterModule$', views.ajouterModule),
	
	url(r'supprimerModule/(?P<idModele>\d+)/(?P<idModule>\d+)$', views.supprimerModule),
	
	
	url(r'paramModifierBAC/(?P<idBAC>\d+)$', views.paramModifierBAC),
	url(r'modifierBAC$', views.modifierBAC),
	url(r'supprimerBAC/(?P<idBAC>\d+)$', views.supprimerBAC),
	
	
	url(r'paramModifierCursus/(?P<idCursus>\d+)$', views.paramModifierCursus),
	url(r'modifierCursus$', views.modifierCursus),
	url(r'supprimerCursus/(?P<idCursus>\d+)$', views.supprimerCursus),
	
	url(r'formation$', views.homeFormation),
	url(r'BAC$',views.homeBAC),
	url(r'cursus$', views.homeCursus),
	url(r'$', views.homeFormation),
]
