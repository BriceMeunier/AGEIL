# coding: utf8
from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'listeEtudiant', views.home),
	
	url(r'(?P<num>\d+)/modifierEtudiantInfoGestion$', views.modifierEtudiantInfoGestion),
	url(r'(?P<num>\d+)/modifierEtudiantInfoPerso$', views.modifierEtudiantInfoPerso),
	url(r'(?P<num>\d+)/modifierEtudiantInfoBAC$', views.modifierEtudiantInfoBAC),
	
	url(r'(?P<num>\d+)/modifierStatutEtudiant$', views.modifierStatutEtudiant),
	
	url(r'(?P<num>\d+)/ajouterCursusPreDUT$', views.ajouterCursusPreDUT),
	url(r'(?P<num>\d+)/ajouterCursusPostDUT$', views.ajouterCursusPostDUT),
	
	url(r'(?P<idCursus>\d+)/modifierCursusPreDUT$', views.modifierCursusPreDUT),
	url(r'(?P<idCursus>\d+)/modifierCursusPostDUT$', views.modifierCursusPostDUT),
	
	url(r'(?P<idCursus>\d+)/supprimerCursusPreDUT$', views.supprimerCursusPreDUT),
	url(r'(?P<idCursus>\d+)/supprimerCursusPostDUT$', views.supprimerCursusPostDUT),
	
	url(r'(?P<num>\d+)/supprimerEtudiant$', views.supprimerEtudiant),
	
	url(r'(?P<num>\d+)/modifierVersionSemestreActuelEtudiant$', views.modifierVersionSemestreActuelEtudiant),
	url(r'(?P<num>\d+)/modifierModeleFormationDuSemestreAnnualise$', views.modifierModeleFormationDuSemestreAnnualise),
	url(r'(?P<num>\d+)/modifierMoyenne/(?P<idSemestreAnnualise>\d+)$', views.modifierMoyenne),
	
    url(r'(?P<num>\d+)$', views.consulterEtudiant),
	
	url(r'ajouterEtudiantInfoGestion$', views.ajouterEtudiantInfoGestion),
	url(r'ajouterEtudiantInfoPerso$', views.ajouterEtudiantInfoPerso),
	url(r'ajouterEtudiantInfoBAC$', views.ajouterEtudiantInfoBAC),
	
	url(r'ajouterEtudiant$', views.ajouterEtudiant),
	
	url(r'paramImportEtudiant$', views.paramImportEtudiant),
    url(r'importerEtudiant$', views.verifImportEtudiantSemestreUnique),
	
	url(r'paramImportMoyenne$', views.paramImportMoyenne),
    url(r'importerMoyenne$', views.importerMoyenne),
]
