# coding: utf8
from django.shortcuts import render
from home.models import Etudiant, StatutEtudiant, SemestreAnnualise, SemestrePossible, EtatSemestrePossible, Association_Etudiant_SemestreAnnualise, ResultatSemestrePossible, DevenirEtudiantPossible, Sexe, ModeleFormation, AnneeUniversitaire, ModeleSemestre, ModeleUE, ModeleModule, MoyenneModule, MoyenneUE, MoyenneSemestre, BAC, MentionBACPossible, CursusPreDUT, CursusPostDUT
from home.forms import EtudiantForm, EtudiantFormAll, CursusPreDUTForm, CursusPostDUTForm
import datetime
import csv

# Create your views here.

#Consultation d'un étudiant.
def home(request):
	return homeMessage(request, False)

def homeMessage(request, messageAAfficher):
	
	#semestresActuels = 
	
	semestresActuel = SemestreAnnualise.objects.exclude(etatSemestre=EtatSemestrePossible.objects.get(etat='TERMINE')).reverse()
	semestresAnnualises = SemestreAnnualise.objects.all().reverse()
	actuelSemestreDefault = 'ALL'
	ancienSemestreDefault = 'ALL'
	redoublantDefaut = 'all'
	diplomeDefaut = 'all'
	trieDefault = 'numero'
	
	# si on arrive sur cette page avec la methode POST, c'est que l'on souhaite lister les étudiants
	if request.method == 'POST':
		
		# selection des étudiants qui correspondent aux critères de recherches ...
		etudiants = Etudiant.objects.all()
		
		# selon le semestre actuel
		actuelSemestreDefault = request.POST['idActuelSemestre']
		if request.POST['idActuelSemestre'] == 'NA':
			etudiants = etudiants.filter(semestreActuel=None)
		
		elif request.POST['idActuelSemestre'] != 'ALL':
			etudiants = etudiants.filter(semestreActuel=SemestreAnnualise.objects.get(id=request.POST['idActuelSemestre']))
		
		# selon un ancien semestre
		ancienSemestreDefault = request.POST['idAncienSemestre']
		if request.POST['idAncienSemestre'] != 'ALL':
			sem = SemestreAnnualise.objects.get(id=request.POST['idAncienSemestre'])
			for etu in etudiants:
				try:
					Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etu, semestre=sem)
				
				except Association_Etudiant_SemestreAnnualise.DoesNotExist:
					# on doit retiré cet étudiant de la liste
					etudiants = etudiants.exclude(id=etu.id)
			
		# selon si ils sont redoublant
		redoublantDefaut = request.POST['redoublant']
		if(redoublantDefaut == 'oui'):
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='diplomé'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='réorienté'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='normal'))
			
		elif(redoublantDefaut == 'non'):
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant diplomé'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant réorienté'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant'))
		
		# selon si ils sont à l'iut, diplomé, réorienté ou n'importe
		diplomeDefaut = request.POST['diplome']
		if(diplomeDefaut == 'reo'):
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant diplomé'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='diplomé'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='normal'))
		
		elif(diplomeDefaut == 'iut'):
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant diplomé'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant réorienté'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='diplomé'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='réorienté'))
		
		elif(diplomeDefaut == 'diplo'):
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant réorienté'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='réorienté'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='redoublant'))
			etudiants = etudiants.exclude(statut=StatutEtudiant.objects.get(statut='normal'))
		
		# trie des étudiants
		trieDefault = request.POST['trie']
		
		# pas la peine de trier si il n'y a aucun étudiant à tier !!!
		if etudiants:
			if request.POST['trie'] == 'numero':
				# pour afficher les étudiant trié par leur numéro
				etudiants = etudiants.order_by('numero')
			elif request.POST['trie'] == 'nomPrenom':
				# pour afficher les étudiant trié par leur nom puis prenom
				etudiants = etudiants.order_by('nom', 'prenom')
		
	return render(request, 'etudiantManager/listeEtudiants.html', locals())

def consulterEtudiant(request, num):
	return consulterEtudiantMessage(request, num, False)

def consulterEtudiantMessage(request, num, messageAAfficher):
	
	# On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
		
		if(etudiant.semestreActuel != None):
		
			modeleFormation = getModeleFormation(etudiant)
			listeVersionSemestre = ModeleSemestre.objects.filter(modeleFormation=modeleFormation, typeSemestre=etudiant.semestreActuel.semestre)
			
			if(etudiant.statut.statut == 'normal'):
				texteModifierStatut = 'Passer à l\'état réorienté'
			
			elif(etudiant.statut.statut == 'redoublant'):
				texteModifierStatut = 'Passer à l\'état redoublant réorienté'
			
			elif(etudiant.statut.statut == 'réorienté'):
				texteModifierStatut = 'Passer à l\'état normal'
			
			else:
				texteModifierStatut = 'Passer à l\'état redoublant'
			
			if len(listeVersionSemestre) > 1:
				modeleSemestreCorrespondant = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=etudiant.semestreActuel).modeleSemestre
				plusieurModeleSemestreCorrespondant = True
			else:
				modeleSemestreCorrespondant = 0
				plusieurModeleSemestreCorrespondant = False
			
			# on determine si on a le droit de modifier le modèle de formation du semestre (interdit si il y a des moyennes qui correspondent à ce semestre)
			ms = MoyenneSemestre.objects.filter(semestreAnnualise=etudiant.semestreActuel)
			if len(ms) > 0:
				droitDeModifierModeleFormationDuSemestreAnnualise = False
			else:
				droitDeModifierModeleFormationDuSemestreAnnualise = True
			
			# on determine si on a le droit de modifier la version du semestre actuel de l'étudiant
			try:
				ms = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=etudiant.semestreActuel)
				
				# l'étudiant a déjà une moyenne pour le semestre actuel, on ne peut donc pas le modifier
				droitDeModifierVersionSemestreActuelEtudiant = False
				
			except MoyenneSemestre.DoesNotExist:
				droitDeModifierVersionSemestreActuelEtudiant = True
		
		# Cursus
		cursusPreDUT = CursusPreDUT.objects.filter(etudiant=etudiant)
		cursusPostDUT = CursusPostDUT.objects.filter(etudiant=etudiant)
		
		# Les différents tableaux de note qui serons affiché
		tableaux = []
		
		assocs = Association_Etudiant_SemestreAnnualise.objects.filter(etudiant=etudiant)
		semestres = []
		for asso in assocs:
			semestres.append(asso.semestre)
		
		for semestre in semestres:
			
			tableau = TableauMoyenne()
			tableau.semestreAnnualise = semestre
			
			# on essaye de récupéré la moyenne liée à ce semestre, si elle n'existe pas, c'est que les notes ne sont pas encore importé !! Donc on a rien à afficher
			try:
				
				tableau.ligneEntete = 'Moyennes du semestre ' + semestre.__str__()
				tableau.listeDivisionUE = []
				tableau.nbUE = 0
				
				moyenneSemestre = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestre)
				
				# la moyenne du semestre
				tableau.moyenneSemestre = moyenneSemestre.moyenne
				
				# on récupère toutes nos moyennes d'UE
				moyennesUE = MoyenneUE.objects.filter(etudiant=etudiant, semestreAnnualise=semestre)
				
				# on récupère toutes nos moyennes de module
				moyennesModuleTmp = MoyenneModule.objects.filter(etudiant=etudiant, semestreAnnualise=semestre)
				
				for moyenneUE in moyennesUE:
					# le modeleUE liée à cette moyenne
					modeleUE = moyenneUE.modeleUE
					
					#nouvelle ligne du tableau avec la moyenne de l'UE
					divUE = DivisionUE()
					divUE.labelUE = moyenneUE.modeleUE.code + ' : ' + moyenneUE.modeleUE.intitule
					divUE.moyenneUE = moyenneUE.moyenne
					divUE.listeDivisionModule = []
					divUE.nbModule = 0
					
					# on s'occupe de tout les module de cette UE avant de passer à l'UE suivante
					moyennesModule = []
					for m in moyennesModuleTmp:
						
						# on ne prend en compte que les modules qui ont pour UE parent l'UE courrante
						if m.modeleModule.modeleUE == modeleUE:
							moyennesModule.append(m)
						
					# on crée une ligne pour chaque module de cette UE
					for moyenneModule in moyennesModule:
						divModule = DivisionModule()
						divModule.labelModule = moyenneModule.modeleModule.code + ' : ' + moyenneModule.modeleModule.intitule
						divModule.moyenneModule = moyenneModule.moyenne
						divModule.numeroModule = divUE.nbModule
						divUE.listeDivisionModule.append(divModule)
						divUE.nbModule = divUE.nbModule + 1
					
					divUE.numeroUE = tableau.nbUE
					tableau.listeDivisionUE.append(divUE)
					tableau.nbUE = tableau.nbUE + 1
				
				# on ajoute le tableau de moyennes que l'on vient de créer à la liste des tableaux de moyennes de l'étudiant
				tableaux.append(tableau)
			
			except MoyenneSemestre.DoesNotExist:
				tableau.ligneEntete = 'PAS_DE_MOYENNE'
				tableaux.append(tableau)
		
		
	# Si l'étudiant n'existe pas, on affiche une erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de consultation'
		messageErreur = 'L\'étudiant que vous souhaitez consulter n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Aucun étudiant avec le numéro ' + num + ' n\'est référencée dans la base.')
		return render(request, 'erreurUtilisateur.html', locals())
		
	return render(request, 'etudiantManager/etudiant.html', locals())

def ajouterEtudiantInfoGestion(request):
	
	semestresPossible = SemestrePossible.objects.all()
	
	anneesUniversitaires = []
	anneeCivileActuel = datetime.datetime.now().year
	for x in range(0, 5):
		anneesUniversitaires.append(str(anneeCivileActuel - x + 1) + '/' + str(anneeCivileActuel - x + 2))
	
	anneeParDefaut = anneesUniversitaires[1]
	
	return render(request, 'etudiantManager/ajouterEtudiantInfoGestion.html', locals())

def ajouterEtudiantInfoPerso(request):
	
	# on regarde si il y a déjà un étudiant avec ce numéro
	try:
		etu = Etudiant.objects.get(numero=request.POST['numero'])
		
		# il existe déjà un étudiant avec ce numero !!!
		titreErreur = 'Erreur d\'ajout de l\'étudiant'
		messageErreur = 'Le numéro étudiant est un identifiant unique, il ne peut pas y avoir plusieurs étudiants avec le même numéro.'
		detailsErreur = []
		detailsErreur.append('Il existe déjà un étudiant ayant le numéro ' + str(etu.numero)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	except Etudiant.DoesNotExist:
		etu = None
	
	# on teste si l'AnneeUniversitaire saisie existe, sinon on la rajoute.
	try:
		anneeUniversitaire = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['anneeUniversitaire'])
		
	except AnneeUniversitaire.DoesNotExist:
		anneeUniversitaire = AnneeUniversitaire(anneeUniversitaire=request.POST['anneeUniversitaire'])
		anneeUniversitaire.save()
	
	semestrePossible = SemestrePossible.objects.get(id=request.POST['semestre'])
	
	# on teste si le SemestreAnnualise correspondant à l'AnneeUniversitaire et au SemestrePossible saisie existe, sinon on le rajoute.
	try:
		semestreActuel = SemestreAnnualise.objects.get(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire)
		
	except SemestreAnnualise.DoesNotExist:
		
		# le semestreAnnualisé n'existe pas, il faut donc l'ajouter et préciser à quel modèle de formation il est rattaché
		# on teste si on a déjà fait l'étape de choix du modèle de formation
		if 'modeleFormation' in request.POST.keys():
			
			modeleFormation = ModeleFormation.objects.get(id=request.POST['modeleFormation'])
			
			etatSemestre = EtatSemestrePossible.objects.get(etat='EN_COURS')
			SemestreAnnualise(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire, modeleFormation=modeleFormation, etatSemestre=etatSemestre).save()
			semestreActuel = SemestreAnnualise.objects.get(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire)
		
		# sinon on envoie sur la page de choix du modèle de formation
		else:
			modelesFormation = ModeleFormation.objects.all()
			
			infosPOST = []
			for name in request.POST.keys():
				kv = KeyValue()
				kv.key = name
				kv.value = request.POST[name]
				infosPOST.append(kv)
			
			return render(request, 'etudiantManager/creerSemestreAnnualise.html', locals())
	
	form = EtudiantForm()
	
	infosPOST = []
	for name in request.POST.keys():
		if name != 'csrfmiddlewaretoken' and name != 'anneeUniversitaire' and name != 'semestre' and name != 'modeleFormation':
			kv = KeyValue()
			kv.key = name
			kv.value = request.POST[name]
			infosPOST.append(kv)
	
	kv = KeyValue()
	kv.key = 'semestreActuel'
	kv.value = semestreActuel.id
	infosPOST.append(kv)
	
	return render(request, 'etudiantManager/ajouterEtudiantInfoPerso.html', locals())

def ajouterEtudiantInfoBAC(request):
	
	BACs = BAC.objects.all()
	mentions = MentionBACPossible.objects.all()
	anneesUniversitaires = []
	anneeCivileActuel = datetime.datetime.now().year
	for x in range(0, 15):
		anneesUniversitaires.append(str(anneeCivileActuel - x - 1) + '/' + str(anneeCivileActuel - x))
	
	anneeParDefaut = anneesUniversitaires[0]
	
	# on garde les infos du post précédent
	infosPOST = []
	for name in request.POST.keys():
		if name != 'csrfmiddlewaretoken':
			kv = KeyValue()
			kv.key = name
			kv.value = request.POST[name]
			infosPOST.append(kv)
	
	# on ajoute le statut normal à l'étudiant
	infoStatut = KeyValue()
	infoStatut.key = 'statut'
	infoStatut.value = StatutEtudiant.objects.get(statut='normal').id
	infosPOST.append(infoStatut)
	
	return render(request, 'etudiantManager/ajouterEtudiantInfoBAC.html', locals())

def ajouterEtudiant(request):
	
	# on regarde si il y a déjà un étudiant avec ce numéro (2ème vérification)
	try:
		etu = Etudiant.objects.get(numero=request.POST['numero'])
		
		# il existe déjà un étudiant avec ce numero !!!
		titreErreur = 'Erreur d\'ajout de l\'étudiant'
		messageErreur = 'Le numéro étudiant est un identifiant unique, il ne peut pas y avoir plusieurs étudiants avec le même numéro.'
		detailsErreur = []
		detailsErreur.append('Il existe déjà un étudiant ayant le numéro ' + str(etu.numero)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	except:
		a = None # pour pas que ça bug
	
	# on regarde si on a renseigné l'annee du BAC
	if request.POST['anneeDuBAC'] == '':
		anneeBAC = None
		
	else:
	
		# on regarde si l'année du BAC renseigné existe
		try:
			anneeBAC = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['anneeDuBAC'])
		
		except AnneeUniversitaire.DoesNotExist:
			anneeBAC = AnneeUniversitaire(anneeUniversitaire=request.POST['anneeDuBAC'])
			anneeBAC.save()
		
	form = EtudiantFormAll(request.POST)
	
	if form.is_valid():
		etudiant = form.save(commit=False)
		etudiant.anneeBAC = anneeBAC
		etudiant.save()
		
		messageAAfficher = False
		
		modeleFormation = getModeleFormation(etudiant)
		
		# on récupère une liste des ModeleSemestre du ModeleFormation que l'on attribut à l'étudiant du typeSemestre auquel l'étudiant est ajouté
		modelesSemestre = ModeleSemestre.objects.filter(modeleFormation=modeleFormation, typeSemestre=etudiant.semestreActuel.semestre)
		
		# par défaut, on prend le premier élément de cette liste
		modeleSemestre = modelesSemestre[0]
		
		# si il y a plusieurs modèles, on affiche un message pour dire celui mis par défaut
		if len(modelesSemestre) > 1:
			messageAAfficher = 'Attention ! Il y a plusieur modèles de semestre qui correspondent au semestre de type ' + etudiant.semestreActuel.semestre.__str__() + ' pour le modèle ' + modeleFormation.__str__() + '. L\'étudiant a été rattaché par défaut au modèle de semestre ' + modeleSemestre.__str__() + '. Vous pouvez modifier le modèle de semestre de l\'étudiant en cliquant sur modifier à la ligne correspondante.'
		
		# on crée le lien durable dans la base entre l'étudiant et le semestre avec Association_Etudiant_SemestreAnnualise
		resultat = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
		devenir = DevenirEtudiantPossible.objects.get(code='ATTENTE_DE_DECISION')
		Association_Etudiant_SemestreAnnualise(etudiant=etudiant, semestre=etudiant.semestreActuel, modeleSemestre=modeleSemestre, resultatSemestreCalcule=resultat, resultatSemestrePreJury=resultat, resultatSemestreJury=resultat, devenirEtudiant=devenir).save()
		
		return consulterEtudiantMessage(request, etudiant.numero, messageAAfficher)
	
def modifierEtudiantInfoGestion(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		
		messageAAfficher = 'L\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom + ' a bien été modifié.'
		
		# MISE A JOUR du numero ...
		
		# si on a modifié le numero de l'étudiant
		if request.POST['numero'] != num:
			
			# on regarde si il y a déjà un étudiant avec ce numéro
			try:
				etu = Etudiant.objects.get(numero=request.POST['numero'])
				
				# il existe déjà un étudiant avec ce numero -> Erreur !!!
				titreErreur = 'Erreur de modifiaction de l\'étudiant'
				messageErreur = 'Le numéro étudiant est un identifiant unique, il ne peut pas y avoir plusieurs étudiants avec le même numéro.'
				detailsErreur = []
				detailsErreur.append('Il existe déjà un étudiant ayant le numéro ' + str(etu.numero)+ '.')
				return render(request, 'erreurUtilisateur.html', locals())
			
			except Etudiant.DoesNotExist:
				etudiant.numero = request.POST['numero']
		
		# MISE A JOUR du semestreActuel ...
		
		# on teste si l'AnneeUniversitaire saisie existe, sinon on la rajoute.
		try:
			anneeUniversitaire = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['anneeUniversitaire'])
			
		except AnneeUniversitaire.DoesNotExist:
			anneeUniversitaire = AnneeUniversitaire(anneeUniversitaire=request.POST['anneeUniversitaire'])
			anneeUniversitaire.save()
		
		semestrePossible = SemestrePossible.objects.get(id=request.POST['semestre'])
		
		# on teste si le SemestreAnnualise correspondant à l'AnneeUniversitaire et au SemestrePossible saisie existe, sinon on le rajoute.
		try:
			semestreActuel = SemestreAnnualise.objects.get(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire)
			
		except SemestreAnnualise.DoesNotExist:
			
			# le semestreAnnualisé n'existe pas, il faut donc l'ajouter et préciser à quel modèle de formation il est rattaché
			# on teste si on a déjà fait l'étape de choix du modèle de formation
			if 'modeleFormation' in request.POST.keys():
				
				modeleFormation = ModeleFormation.objects.get(id=request.POST['modeleFormation'])
				
				etatSemestre = EtatSemestrePossible.objects.get(etat='EN_COURS')
				SemestreAnnualise(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire, modeleFormation=modeleFormation, etatSemestre=etatSemestre).save()
				semestreActuel = SemestreAnnualise.objects.get(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire)
			
			# sinon on envoie sur la page de choix du modèle de formation
			else:
				modelesFormation = ModeleFormation.objects.all()
				
				infosPOST = []
				for name in request.POST.keys():
					kv = KeyValue()
					kv.key = name
					kv.value = request.POST[name]
					infosPOST.append(kv)
				
				return render(request, 'etudiantManager/creerSemestreAnnualise.html', locals())
		
		#il ne faut pas oublier de mettre à jour l'Association_Etudiant_SemestreAnnualise !!!
		assoc = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=etudiant.semestreActuel)
		
		# 1) le semestre de l'association
		assoc.semestre = semestreActuel
		
		# 2) le modèle de semestre de l'association
		modeleFormation = semestreActuel.modeleFormation
		
		# on récupère une liste des ModeleSemestre du ModeleFormation que l'on attribut à l'étudiant du typeSemestre auquel l'étudiant est ajouté
		modelesSemestre = ModeleSemestre.objects.filter(modeleFormation=modeleFormation, typeSemestre=semestreActuel.semestre)
		
		# par défaut, on prend le premier élément de cette liste
		modeleSemestre = modelesSemestre[0]
		
		assoc.modeleSemestre = modeleSemestre
		
		# 3) on sauvegarde l'association
		assoc.save()
		
		etudiant.semestreActuel = semestreActuel
		
		# si il y a plusieurs modèles, on affiche un message pour dire celui mis par défaut
		if len(modelesSemestre) > 1:
			messageAAfficher = messageAAfficher + ' Attention ! Il y a plusieur modèles de semestre qui correspondent au semestre de type ' + etudiant.semestreActuel.semestre.__str__() + ' pour le modèle ' + modeleFormation.__str__() + '. L\'étudiant a été rattaché par défaut au modèle de semestre ' + modeleSemestre.__str__() + '. Vous pouvez modifier le modèle de semestre de l\'étudiant en cliquant sur modifier à la ligne correspondante.'
		
		# on a modifier toutes les info de gestion de l'étudiant, on peut le sauvegarder
		etudiant.save()
		
		return consulterEtudiantMessage(request, etudiant.numero, messageAAfficher)
		
	else:
		
		try:
			ms = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=etudiant.semestreActuel)
			modeleFormationModifiable = False
		
		except MoyenneSemestre.DoesNotExist:
			modeleFormationModifiable = True
		
		semestresPossible = SemestrePossible.objects.all()
		
		anneesUniversitaires = []
		anneeCivileActuel = datetime.datetime.now().year
		for x in range(0, 5):
			anneesUniversitaires.append(str(anneeCivileActuel - x + 1) + '/' + str(anneeCivileActuel - x + 2))
		
		return render(request, 'etudiantManager/modifierEtudiantInfoGestion.html', locals())
	
def modifierEtudiantInfoPerso(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		# on reçois le formulaire modifié
		form = EtudiantForm(request.POST, instance=etudiant)
		
		if form.is_valid():
			etudiant = form.save()
			
			messageAAfficher = 'L\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom + ' a bien été modifié.'
			
			return consulterEtudiantMessage(request, num, messageAAfficher)
			
		else:
			titreErreur = 'Erreur de modification de l\'étudiant'
			messageErreur = 'Il semblerais que l\'une des modification que vous souhaitez effectuer ne soit pas possible. Etes-vous certain de ne pas avoir fait d\'erreur en saisissant les informations ?'
			detailsErreur = []
			return render(request, 'erreurUtilisateur.html', locals())
			
	else:
		
		form = EtudiantForm(instance=etudiant)
		
		return render(request, 'etudiantManager/modifierEtudiantInfoPerso.html', locals())
	
def modifierEtudiantInfoBAC(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		
		if request.POST['BAC'] == '':
			etudiant.BAC = None
		else:
			etudiant.BAC = BAC.objects.get(id=request.POST['BAC'])
		
		if request.POST['anneeBAC'] == '':
			anneeBAC = None
			
		else:
		
			# on regarde si l'année du bac renseigné existe
			try:
				anneeBAC = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['anneeBAC'])
			
			except AnneeUniversitaire.DoesNotExist:
				anneeBAC = AnneeUniversitaire(anneeUniversitaire=request.POST['anneeBAC'])
				anneeBAC.save()
			
		etudiant.anneeBAC = anneeBAC
		
		if request.POST['mentionBAC'] == '':
			mentionBAC = None
			
		else:
			mentionBAC = MentionBACPossible.objects.get(id=request.POST['mentionBAC'])
		
		etudiant.mentionBAC = mentionBAC
		
		etudiant.save()
		
		messageAAfficher = 'L\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom + ' a bien été modifié.'
		
		return consulterEtudiantMessage(request, num, messageAAfficher)
		
	else:
		
		BACs = BAC.objects.all()
		mentions = MentionBACPossible.objects.all()
		anneesUniversitaires = []
		anneeCivileActuel = datetime.datetime.now().year
		for x in range(0, 15):
			anneesUniversitaires.append(str(anneeCivileActuel - x - 1) + '/' + str(anneeCivileActuel - x))
		
		return render(request, 'etudiantManager/modifierEtudiantInfoBAC.html', locals())

def modifierStatutEtudiant(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	if(etudiant.statut.statut == 'redoublant'):
		etudiant.statut = StatutEtudiant.objects.get(statut='redoublant réorienté')
		
	elif(etudiant.statut.statut == 'redoublant réorienté'):
		etudiant.statut = StatutEtudiant.objects.get(statut='redoublant')
	
	elif(etudiant.statut.statut == 'réorienté'):
		etudiant.statut = StatutEtudiant.objects.get(statut='normal')
	
	else:
		etudiant.statut = StatutEtudiant.objects.get(statut='réorienté')
	
	etudiant.save()
	
	message = 'Le statut a bien été modifié.'
	
	return consulterEtudiantMessage(request, num, message)
	
def ajouterCursusPreDUT(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		
		form = CursusPreDUTForm(request.POST)
		
		if form.is_valid():
			
			cursus = form.save(commit=False)
			cursus.etudiant = etudiant
			
			# on regarde si l'année renseigné existe
			try:
				annee = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['annee'])
			
			except AnneeUniversitaire.DoesNotExist:
				annee = AnneeUniversitaire(anneeUniversitaire=request.POST['annee'])
				annee.save()
			
			cursus.anneeUniversitaire = annee
			
			cursus.save()
			
			messageAAfficher = 'Le cursus ' + cursus.__str__() + ' a bien été ajouté.'
			
			return consulterEtudiantMessage(request, num, messageAAfficher)
			
		else:
			
			titreErreur = 'Erreur d\'ajout du cursus'
			messageErreur = 'Il semblerais que les informations renseignées soit incorrectes. Veuillez vérifier votre saisie.'
			detailsErreur = []
			return render(request, 'erreurUtilisateur.html', locals())
		
	else:
		
		anneesUniversitaires = []
		anneeCivileActuel = datetime.datetime.now().year
		for x in range(0, 15):
			anneesUniversitaires.append(str(anneeCivileActuel - x - 1) + '/' + str(anneeCivileActuel - x))
		
		form = CursusPreDUTForm()
		
		return render(request, 'etudiantManager/ajouterCursusPreDUT.html', locals())
	
def ajouterCursusPostDUT(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		
		form = CursusPostDUTForm(request.POST)
		
		if form.is_valid():
			
			cursus = form.save(commit=False)
			cursus.etudiant = etudiant
			
			# on regarde si l'année renseigné existe
			try:
				annee = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['annee'])
			
			except AnneeUniversitaire.DoesNotExist:
				annee = AnneeUniversitaire(anneeUniversitaire=request.POST['annee'])
				annee.save()
			
			cursus.anneeUniversitaire = annee
			
			cursus.save()
			
			messageAAfficher = 'Le cursus ' + cursus.__str__() + ' a bien été ajouté.'
			
			return consulterEtudiantMessage(request, num, messageAAfficher)
			
		else:
			
			titreErreur = 'Erreur d\'ajout du cursus'
			messageErreur = 'Il semblerais que les informations renseignées soit incorrectes. Veuillez vérifier votre saisie.'
			detailsErreur = []
			return render(request, 'erreurUtilisateur.html', locals())
		
	else:
		
		anneesUniversitaires = []
		anneeCivileActuel = datetime.datetime.now().year
		for x in range(0, 15):
			anneesUniversitaires.append(str(anneeCivileActuel - x - 1) + '/' + str(anneeCivileActuel - x))
		
		form = CursusPostDUTForm()
		
		return render(request, 'etudiantManager/ajouterCursusPostDUT.html', locals())
	
def modifierCursusPreDUT(request, idCursus):
	
	try:
		cursus = CursusPreDUT.objects.get(id=idCursus)
		
	except CursusPreDUT.DoesNotExist:
		titreErreur = 'Erreur de modification du cursus'
		messageErreur = 'Le cursus que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		
		form = CursusPreDUTForm(request.POST, instance=cursus)
		
		if form.is_valid():
			cursus = form.save(commit=False)
			
			# on regarde si l'année renseigné existe
			try:
				annee = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['annee'])
			
			except AnneeUniversitaire.DoesNotExist:
				annee = AnneeUniversitaire(anneeUniversitaire=request.POST['annee'])
				annee.save()
			
			cursus.anneeUniversitaire = annee
			
			cursus.save()
			
			messageAAfficher = 'Le cursus ' + cursus.__str__() + ' a bien été modifié.'
			
			return consulterEtudiantMessage(request, cursus.etudiant.numero, messageAAfficher)
		
	else:
		
		anneesUniversitaires = []
		anneeCivileActuel = datetime.datetime.now().year
		for x in range(0, 15):
			anneesUniversitaires.append(str(anneeCivileActuel - x - 1) + '/' + str(anneeCivileActuel - x))
		
		form = CursusPreDUTForm(instance=cursus)
		
		return render(request, 'etudiantManager/modifierCursusPreDUT.html', locals())

def modifierCursusPostDUT(request, idCursus):
	
	try:
		cursus = CursusPostDUT.objects.get(id=idCursus)
		
	except CursusPostDUT.DoesNotExist:
		titreErreur = 'Erreur de modification du cursus'
		messageErreur = 'Le cursus que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST':
		
		form = CursusPostDUTForm(request.POST, instance=cursus)
		
		if form.is_valid():
			cursus = form.save(commit=False)
			
			# on regarde si l'année renseigné existe
			try:
				annee = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['annee'])
			
			except AnneeUniversitaire.DoesNotExist:
				annee = AnneeUniversitaire(anneeUniversitaire=request.POST['annee'])
				annee.save()
			
			cursus.anneeUniversitaire = annee
			
			cursus.save()
			
			messageAAfficher = 'Le cursus ' + cursus.__str__() + ' a bien été modifié.'
			
			return consulterEtudiantMessage(request, cursus.etudiant.numero, messageAAfficher)
		
	else:
		
		anneesUniversitaires = []
		anneeCivileActuel = datetime.datetime.now().year
		for x in range(0, 15):
			anneesUniversitaires.append(str(anneeCivileActuel - x - 1) + '/' + str(anneeCivileActuel - x))
		
		form = CursusPostDUTForm(instance=cursus)
		
		return render(request, 'etudiantManager/modifierCursusPostDUT.html', locals())
	
def supprimerCursusPreDUT(request, idCursus):
	
	try:
		cursus = CursusPreDUT.objects.get(id=idCursus)
		etudiant = cursus.etudiant
		
	except CursusPreDUT.DoesNotExist:
		titreErreur = 'Erreur de suppression du cursus'
		messageErreur = 'Le cursus que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
		
		messageAAfficher = 'Le cursus a bien été supprimé.'
		num = cursus.etudiant.numero
		cursus.delete()
		request.method = 'GET'
		
		return consulterEtudiantMessage(request, num, messageAAfficher)
	
	else:
		
		actionUtilisateur = 'Supprimer le cursus ' + cursus.__str__() + ' pour l\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom
		lienActionUtilisateur = '/gestionEtudiant/' + str(cursus.id) + '/supprimerCursusPreDUT'
		return render(request, 'pageConfirmation.html', locals())
	
def supprimerCursusPostDUT(request, idCursus):
	
	try:
		cursus = CursusPostDUT.objects.get(id=idCursus)
		etudiant = cursus.etudiant
		
	except CursusPostDUT.DoesNotExist:
		titreErreur = 'Erreur de suppression du cursus'
		messageErreur = 'Le cursus que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
		
		messageAAfficher = 'Le cursus a bien été supprimé.'
		num = cursus.etudiant.numero
		cursus.delete()
		request.method = 'GET'
		
		return consulterEtudiantMessage(request, num, messageAAfficher)
	
	else:
		
		actionUtilisateur = 'Supprimer le cursus ' + cursus.__str__() + ' pour l\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom
		lienActionUtilisateur = '/gestionEtudiant/' + str(cursus.id) + '/supprimerCursusPostDUT'
		return render(request, 'pageConfirmation.html', locals())
	
def modifierVersionSemestreActuelEtudiant(request, num):
	
	#On essaie de récupérer l'étudiant dont le numero est passé en paramètre.
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une Erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de modification de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez modifier n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
		
		
	if request.method == 'POST':
		
		modeleSemestre = ModeleSemestre.objects.get(id=request.POST['idModeleSemestre'])
		
		# on modifie la version du semestre (dans la table Association_Etudiant_SemestreAnnualise)
		assoc = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=etudiant.semestreActuel)
		assoc.modeleSemestre = modeleSemestre
		assoc.save()
		
		messageAAfficher = 'Cet étudiant suit maintenant le modèle de semestre ' + modeleSemestre.__str__() + '.'
		
		return consulterEtudiantMessage(request, num, messageAAfficher)
	
	else:
		
		# on crée les choix de semestre
		modelesSemestre = ModeleSemestre.objects.filter(modeleFormation=getModeleFormation(etudiant), typeSemestre=etudiant.semestreActuel.semestre)
		modeleDefaut = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=etudiant.semestreActuel).modeleSemestre
		
	return render(request, 'etudiantManager/modifierVersionSemestreActuelEtudiant.html', locals())
	
def modifierModeleFormationDuSemestreAnnualise(request, num):
	
	semestreAnnualise = Etudiant.objects.get(numero=num).semestreActuel
	
	if request.method == 'POST':
		
		modeleFormation = ModeleFormation.objects.get(id=request.POST['idModeleFormation'])
		semestreAnnualise.modeleFormation = modeleFormation
		semestreAnnualise.save()
		
		# on met à jour toutes les associations
		assocs = Association_Etudiant_SemestreAnnualise.objects.filter(semestre=semestreAnnualise)
		
		# on récupère une liste des ModeleSemestre du ModeleFormation que l'on attribut à l'étudiant du typeSemestre auquel l'étudiant est ajouté
		modelesSemestre = ModeleSemestre.objects.filter(modeleFormation=modeleFormation, typeSemestre=semestreAnnualise.semestre)
		
		# par défaut, on prend le premier élément de cette liste
		modeleSemestre = modelesSemestre[0]
		
		for assoc in assocs:
			
			assoc.modeleSemestre = modeleSemestre
			
			# on sauvegarde l'association
			assoc.save()
		
		
		messageAAfficher = 'Le modèle de formation pour le semestre ' + semestreAnnualise.__str__() + ' a bien été modifié.'
		
		# si il y a plusieurs modèles, on affiche un message pour dire celui mis par défaut
		if len(modelesSemestre) > 1:
			messageAAfficher = messageAAfficher + ' Attention ! Il y a plusieur modèles de semestre qui correspondent au semestre de type ' + semestreAnnualise.semestre.__str__() + ' pour le modèle ' + modeleFormation.__str__() + '. Les étudiants ont été rattachés par défaut au modèle de semestre ' + modeleSemestre.__str__() + '. Vous pouvez modifier le modèle de semestre d\'un étudiant en cliquant sur modifier à la ligne correspondante.'
		
		return consulterEtudiantMessage(request, num, messageAAfficher)
		
	
	else:
		
		modelesFormation = ModeleFormation.objects.all()
		modeleDefaut = semestreAnnualise.modeleFormation
		
		return render(request, 'etudiantManager/modifierModeleFormationDuSemestreAnnualise.html', locals())

def modifierMoyenne(request, num, idSemestreAnnualise):
	
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de suppression de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num)+ '.')
		return render(request, 'erreurUtilisateur.html', locals())
	
	semestre = SemestreAnnualise.objects.get(id=idSemestreAnnualise)
	
	if request.method == 'POST':
		
		# on essaye de récupéré la moyenne liée à ce semestre, si elle n'existe pas, c'est que les notes ne sont pas encore importé !! Donc on les crées
		try:
			moyenneSemestre = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestre)
			
			# on récupère toutes nos moyennes d'UE
			moyennesUE = MoyenneUE.objects.filter(etudiant=etudiant, semestreAnnualise=semestre)
			
			# on récupère toutes nos moyennes de module
			moyennesModuleTmp = MoyenneModule.objects.filter(etudiant=etudiant, semestreAnnualise=semestre)
		
		except MoyenneSemestre.DoesNotExist:
			
			assoc = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestre)
			moyenneSemestre = MoyenneSemestre(etudiant=etudiant, semestreAnnualise=semestre, modeleSemestre=assoc.modeleSemestre)
			
			modelesUE = ModeleUE.objects.filter(modeleSemestre=assoc.modeleSemestre)
			moyennesUE = []
			
			moyennesModuleTmp = []
			
			for modeleUE in modelesUE:
				moyennesUE.append(MoyenneUE(etudiant=etudiant, semestreAnnualise=semestre, modeleUE=modeleUE))
				
				modelesModule = ModeleModule.objects.filter(modeleUE=modeleUE)
				
				for modeleModule in modelesModule:
					moyennesModuleTmp.append(MoyenneModule(etudiant=etudiant, semestreAnnualise=semestre, modeleModule=modeleModule))
		
		moyenneGenerale = 0.0
		sommeCoefGenerale = 0.0
		
		for moyenneModule in moyennesModuleTmp:
			moyenneModule.moyenne = float(request.POST[moyenneModule.modeleModule.code])
			moyenneModule.save()
		
		for moyenneUE in moyennesUE:
			moyenne = 0.0
			sommeCoef = 0.0
			for moyenneModule in moyennesModuleTmp:
				if moyenneModule.modeleModule.modeleUE == moyenneUE.modeleUE:
					moyenne = moyenne + (moyenneModule.moyenne * moyenneModule.modeleModule.coefficient)
					sommeCoef = sommeCoef + moyenneModule.modeleModule.coefficient
			
			moyenne = moyenne / sommeCoef
			
			moyenneUE.moyenne = moyenne
			moyenneUE.save()
			
			moyenneGenerale = moyenneGenerale + (moyenne * moyenneUE.modeleUE.coefficient)
			sommeCoefGenerale = sommeCoefGenerale + moyenneUE.modeleUE.coefficient
		
		moyenneGenerale = moyenneGenerale / sommeCoefGenerale
		
		moyenneSemestre.moyenne = moyenneGenerale
		moyenneSemestre.save()
		
		messageAAfficher = "Les moyennes de l'étudiant ont été mises à jour."
		
		listeEtudiantsACalculerResultat = []
		listeEtudiantsACalculerResultat.append(etudiant)
		
		calculerResultat(listeEtudiantsACalculerResultat, semestre)
		
		return consulterEtudiantMessage(request, etudiant.numero, messageAAfficher)
		
	else :
		
		tableau = TableauMoyenne()
		tableau.semestreAnnualise = semestre
		tableau.ligneEntete = 'Moyennes du semestre ' + semestre.__str__()
		tableau.ligneTableau = []
		
		# ligne = []
		# ligne.append('Moyenne générale')
		
		# on essaye de récupéré la moyenne liée à ce semestre, si elle n'existe pas, c'est que les notes ne sont pas encore importé !! Donc on les crées
		try:
			moyenneSemestre = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestre)
			
			# on récupère toutes nos moyennes d'UE
			moyennesUE = MoyenneUE.objects.filter(etudiant=etudiant, semestreAnnualise=semestre)
			
			# on récupère toutes nos moyennes de module
			moyennesModuleTmp = MoyenneModule.objects.filter(etudiant=etudiant, semestreAnnualise=semestre)
		
		except MoyenneSemestre.DoesNotExist:
			
			assoc = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestre)
			moyenneSemestre = MoyenneSemestre(etudiant=etudiant, semestreAnnualise=semestre, modeleSemestre=assoc.modeleSemestre)
			
			modelesUE = ModeleUE.objects.filter(modeleSemestre=assoc.modeleSemestre)
			moyennesUE = []
			
			moyennesModuleTmp = []
			
			for modeleUE in modelesUE:
				moyennesUE.append(MoyenneUE(etudiant=etudiant, semestreAnnualise=semestre, modeleUE=modeleUE))
				
				modelesModule = ModeleModule.objects.filter(modeleUE=modeleUE)
				
				for modeleModule in modelesModule:
					moyennesModuleTmp.append(MoyenneModule(etudiant=etudiant, semestreAnnualise=semestre, modeleModule=modeleModule))
			
		# la moyenne du semestre
		# ligne.append(str(moyenneSemestre.moyenne).replace(",", "."))
		# ligne.append("Moyenne générale")
		# tableau.ligneTableau.append(ligne) # fin de la ligne de la moyenne générale
		
		for moyenneUE in moyennesUE:
			# le modeleUE liée à cette moyenne
			modeleUE = moyenneUE.modeleUE
			
			#nouvelle ligne du tableau avec la moyenne de l'UE
			ligne = []
			# ligne.append(moyenneUE.modeleUE.code + ' : ' + moyenneUE.modeleUE.intitule)
			# ligne.append(str(moyenneUE.moyenne).replace(",", "."))
			# ligne.append(moyenneUE.modeleUE.code)
			# tableau.ligneTableau.append(ligne)
			
			# on s'occupe de tout les module de cette UE avant de passer à l'UE suivante
			moyennesModule = []
			for m in moyennesModuleTmp:
				
				# on ne prend en compte que les modules qui ont pour UE parent l'UE courrante
				if m.modeleModule.modeleUE == modeleUE:
					moyennesModule.append(m)
				
			# on crée une ligne pour chaque module de cette UE
			for moyenneModule in moyennesModule:
				ligne = []
				ligne.append(moyenneModule.modeleModule.code + ' : ' + moyenneModule.modeleModule.intitule)
				ligne.append(str(moyenneModule.moyenne).replace(",", "."))
				ligne.append(moyenneModule.modeleModule.code)
				tableau.ligneTableau.append(ligne)
		
		return render(request, 'etudiantManager/modifierMoyenne.html', locals())

def supprimerEtudiant(request, num):
	
	try:
		etudiant = Etudiant.objects.get(numero=num)
	#Si l'étudiant n'existe pas, on affiche une erreur.
	except Etudiant.DoesNotExist:
		titreErreur = 'Erreur de suppression de l\'étudiant'
		messageErreur = 'L\'étudiant que vous souhaitez supprimer n\'existe pas.'
		detailsErreur = []
		detailsErreur.append('Il n\'existe aucun étudiant ayant le numéro ' + str(num) + '.')
		pageRetour = '/gestionEtudiant/listeEtudiant'
		return render(request, 'erreurUtilisateur.html', locals())
	
	if request.method == 'POST' and 'formulaireDeConfirmation' in request.POST:
		
		messageAAfficher = 'L\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom + ' a bien été supprimé.'
		etudiant.delete()
		request.method = 'GET'
		
		return homeMessage(request, messageAAfficher)
	
	else:
		
		actionUtilisateur = 'Supprimer l\'étudiant ' + etudiant.nom + ' ' + etudiant.prenom
		lienActionUtilisateur = '/gestionEtudiant/' + num + '/supprimerEtudiant'
		return render(request, 'pageConfirmation.html', locals())

def paramImportEtudiant(request):
	semestres = SemestrePossible.objects.all()
	
	anneesUniversitaires = []
	anneeCivileActuel = datetime.datetime.now().year
	for x in range(0, 5):
		anneesUniversitaires.append(str(anneeCivileActuel - x + 1) + '/' + str(anneeCivileActuel - x + 2))
	
	anneeParDefaut = anneesUniversitaires[1]
	
	return render(request, 'etudiantManager/paramImportEtudiant.html', locals())

def verifImportEtudiantSemestreUnique(request):
	
	# on récupère le type de semestre des étudiant que l'on veut importer (S1, S2, S3, S4)
	semestrePossible = SemestrePossible.objects.get(id=request.POST['idSemestrePossible'])
	
	# on teste si l'AnneeUniversitaire saisie existe, sinon on la rajoute.
	try:
		anneeUniversitaire = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['anneeUniversitaire'])
		
	except AnneeUniversitaire.DoesNotExist:
		anneeUniversitaire = AnneeUniversitaire(anneeUniversitaire=request.POST['anneeUniversitaire'])
		anneeUniversitaire.save()
		
	# on teste si le SemestreAnnualise correspondant à l'AnneeUniversitaire et au SemestrePossible saisie existe, sinon on le rajoute.
	try:
		semestreActuel = SemestreAnnualise.objects.get(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire)
		
	except SemestreAnnualise.DoesNotExist:
		# le semestreAnnualisé n'existe pas, il faut donc l'ajouter et préciser à quel modèle de formation il est rattaché
		# on teste si on a déjà fait l'étape de choix du modèle de formation
		if 'modeleFormation' in request.POST.keys():
			
			modeleFormation = ModeleFormation.objects.get(id=request.POST['modeleFormation'])
			etatSemestre = EtatSemestrePossible.objects.get(etat='EN_COURS')
			
			SemestreAnnualise(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire, modeleFormation=modeleFormation, etatSemestre=etatSemestre).save()
			semestreActuel = SemestreAnnualise.objects.get(semestre=semestrePossible, anneeUniversitaire=anneeUniversitaire)
		
		# sinon on envoie sur la page de choix du modèle de formation
		else:
			modelesFormation = ModeleFormation.objects.all()
			
			infosPOST = []
			for name in request.POST.keys():
				kv = KeyValue()
				kv.key = name
				kv.value = request.POST[name]
				infosPOST.append(kv)
			
			return render(request, 'etudiantManager/creerSemestreAnnualise.html', locals())
	
	# on récupère une liste des ModeleSemestre du ModeleFormation que l'on attribut à l'étudiant du typeSemestre auquel l'étudiant est ajouté
	modeleFormation = semestreActuel.modeleFormation
	modelesSemestre = ModeleSemestre.objects.filter(modeleFormation=modeleFormation, typeSemestre=semestrePossible)
	
	# par défaut, on prend le premier élément de cette liste
	modeleSemestre = modelesSemestre[0]
	
	# si il y a plusieurs modèles, on affiche un message pour dire celui mis par défaut
	isMessageAAfficher = len(modelesSemestre) > 1
	messageAAfficher = 'Attention ! Il y a plusieur modèles de semestre qui correspondent au semestre de type ' + semestrePossible.__str__() + ' pour le modèle ' + modeleFormation.__str__() + '. Les étudiants ont été rattachés par défaut au modèle de semestre ' + modeleSemestre.__str__() + '. Vous pouvez modifier le modèle de semestre de l\'étudiant en cliquant sur modifier à la ligne correspondante.'
	
	return importerEtudiant(request, semestrePossible, modeleSemestre, isMessageAAfficher, messageAAfficher, anneeUniversitaire, semestreActuel)

def importerEtudiant(request, semestrePossible, modeleSemestre, isMessageAAfficher, messageAAfficher, anneeUniversitaire, semestreActuel):
	
	# pour afficher les détails de l'import ...
	etudiantsAjout = []
	etudiantsModif = []
	nbLigneTraite = 0
	
	# pour attribuer au nouveaux étudiants
	statutNormal = StatutEtudiant.objects.get(statut='normal')
	
	# indices de colonnes du fichier ...
	nomColsPossible = {'numero', 'nom', 'prenom', 'sexe', 'telephone', 'dateNaissance', 'adresse', 'email'}
	indCols = {}
	indCols['numero'] = -1
	indCols['nom'] = -1
	indCols['prenom'] = -1
	indCols['sexe'] = -1
	indCols['telephone'] = -1
	indCols['dateNaissance'] = -1
	indCols['adresse'] = -1
	indCols['email'] = -1
	
	# on ouvre le fichier passé en parametre
	with open(request.POST['fichier'], encoding = "ISO-8859-1") as csvfile:
		ligneReader = csv.reader(csvfile, delimiter=';', quotechar='|')
		for ligne in ligneReader:
			if not ligne:
				# ligne vide, rien à faire mais il faut mettre une instruction sinon ça bug ...
				ligne = []
			elif nbLigneTraite == 0:
				# ligne d'entête du fichier, on s'en sert pour initialiser les valeurs d'indices des colonnes
				numCol = 0
				for enteteColonne in ligne:
				
					if enteteColonne not in nomColsPossible:
						# Erreur, certaine colonne porte un nom qui n'est pas référencé
						titreErreur = 'Erreur d\'importation'
						messageErreur = 'Une colonne du fichier porte une entête qui n\'est pas référencée.'
						detailsErreur = []
						detailsErreur.append('Le fichier \'' + request.POST['fichier'] + ' comporte l\'entête \'' + enteteColonne + '\' mais elle n\'est pas référencée.')
						detailsErreur.append('Liste des entêtes possibles :')
						for x in nomColsPossible:
							detailsErreur.append(x)
						return render(request, 'erreurUtilisateur.html', locals())
						
					indCols[enteteColonne] = numCol
					numCol = numCol + 1
				
				# on a fini de lire l'entete.
				# on regarde si tout les champs requis/indispensable sont présent
				if indCols['numero'] == -1 or indCols['nom'] == -1 or indCols['prenom'] == -1:
					# Erreur, il manque certaines colonnes obligatoire.
					titreErreur = 'Erreur d\'importation'
					messageErreur = 'Il manque certaines colonnes obligatoires. Le fichier .csv doit comporter au moins les 3 entêtes de colonne suivant : numero, nom et prenom.'
					detailsErreur = []
					
					if indCols['numero'] == -1:
						detailsErreur.append('Le fichier ne comporte pas d\'entête \'numero\'')
						
					if indCols['nom'] == -1:
						detailsErreur.append('Le fichier ne comporte pas d\'entête \'nom\'')
						
					if indCols['prenom'] == -1:
						detailsErreur.append('Le fichier ne comporte pas d\'entête \'prenom\'')
					return render(request, 'erreurUtilisateur.html', locals())
				
				nbLigneTraite = nbLigneTraite + 1
			else:
				# on cherche si cet étudiant existe déjà dans la BD
				etudiant = Etudiant.objects.filter(numero=int(ligne[indCols['numero']]))
				
				if not etudiant:
					# si il n'existe pas déjà on l'ajoute
					etudiant = Etudiant(numero=int(ligne[indCols['numero']]), nom=ligne[indCols['nom']], prenom=ligne[indCols['prenom']], semestreActuel=semestreActuel)
					etudiant = attribuerValeurFacultativeEtudiant(etudiant, ligne, indCols)
					etudiant.statut = statutNormal
					etudiantsAjout.append(etudiant)
					etudiant.save()
					
					# on crée le lien durable dans la base entre l'étudiant et le semestre avec Association_Etudiant_SemestreAnnualise
					resultat = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
					devenir = DevenirEtudiantPossible.objects.get(code='ATTENTE_DE_DECISION')
					Association_Etudiant_SemestreAnnualise(etudiant=etudiant, semestre=semestreActuel, modeleSemestre=modeleSemestre, resultatSemestreCalcule=resultat, resultatSemestrePreJury=resultat, resultatSemestreJury=resultat, devenirEtudiant=devenir).save()
					
				else:
					# sinon on met juste à jour ces infos
					etudiant = Etudiant.objects.get(numero=int(ligne[0]))
					etudiant.nom = ligne[1]
					etudiant.prenom = ligne[2]
					etudiant.semestreActuel = semestreActuel
					
					try:
						a = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreActuel)
						a.modeleSemestre = modeleSemestre
						a.save()
					
					# si l'Association_Etudiant_SemestreAnnualise n'existait pas déjà, on la crée
					except Association_Etudiant_SemestreAnnualise.DoesNotExist:
						# on crée le lien durable dans la base entre l'étudiant et le semestre avec Association_Etudiant_SemestreAnnualise
						resultat = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
						devenir = DevenirEtudiantPossible.objects.get(code='ATTENTE_DE_DECISION')
						Association_Etudiant_SemestreAnnualise(etudiant=etudiant, semestre=semestreActuel, modeleSemestre=modeleSemestre, resultatSemestreCalcule=resultat, resultatSemestrePreJury=resultat, resultatSemestreJury=resultat, devenirEtudiant=devenir).save()
					
					etudiant = attribuerValeurFacultativeEtudiant(etudiant, ligne, indCols)
					etudiantsModif.append(etudiant)
					etudiant.save()
				
				
				nbLigneTraite = nbLigneTraite + 1
	
	return render(request, 'etudiantManager/importerEtudiant.html', locals())
	
def attribuerValeurFacultativeEtudiant(etudiant, ligne, indCols):
	if indCols['sexe'] != -1:
		etudiant.sexe = Sexe.objects.get(sexe=ligne[indCols['sexe']])
	
	if indCols['telephone'] != -1:
		etudiant.telephone = ligne[indCols['telephone']]
	
	if indCols['dateNaissance'] != -1:
		etudiant.dateNaissance = ligne[indCols['dateNaissance']]
	
	if indCols['adresse'] != -1:
		etudiant.adresse = ligne[indCols['adresse']]
	
	if indCols['email'] != -1:
		etudiant.email = ligne[indCols['email']]
	
	return etudiant

def paramImportMoyenne(request):
	resultatSemestreEnCours = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
	assoc = Association_Etudiant_SemestreAnnualise.objects.filter(resultatSemestreJury=resultatSemestreEnCours)
	
	# liste des semestre qui sont en cours (on peut donc importer leur notes)
	semestresAnnualises = SemestreAnnualise.objects.exclude(etatSemestre=EtatSemestrePossible.objects.get(etat='TERMINE'))
	
	return render(request, 'etudiantManager/paramImportMoyenne.html', locals())

"""
Importe les moyennes des étudiants.
Prend en compte des éventuelles capitalisations d'UE en écrassant les nouveaux résultats avec les anciens
/!\ Dans l'affichage se sont tout de même les nouvelles notes qui sont affichées (même si elles sont moins bonne)
"""
def importerMoyenne(request):
	
	semestreAnnualise = SemestreAnnualise.objects.get(id=request.POST['idSemestreAnnualise'])
	dansLeTableauDesMoyennes = False
	numLigneDansTableauDesMoyennes = 0
	listeCodeEntete = {}
	
	# pour l'affichage des résultats
	listeLigneTableau = []
	
	# liste des étudiant dont les notes ont été importés pour le calcule du résultat
	listeEtudiantsACalculerResultat = []
	
	# on ouvre le fichier passé en parametre
	with open(request.POST['fichier'], encoding = "ISO-8859-1") as csvfile:
		ligneReader = csv.reader(csvfile, delimiter=';', quotechar='"')
		
		for ligne in ligneReader:
			
			# ligne vide, rien à faire mais il faut mettre une instruction sinon ça bug ...
			if not ligne:
				ligne = []
				
			# ligne qui décrit le semestre (entête avec les code UE/Module)
			elif ligne[0] == '' and ligne[1] == '' and ligne[2] == '' and ligne[3] != '':
				print(ligne)
				# si cette ligne correspond au semestre que l'on souhaite importer
				if ligne[3] == 'Semestre' + semestreAnnualise.semestre.semestre[1]:
					print('semestre egal ' + semestreAnnualise.semestre.semestre[1])
					# on remplis notre liste d'entête qui fait la correspondance entre le code d'entête et le numéro de ça colonne
					numCol = 0
					listeCodeEntete = {}
					
					premiereLigneTableau = []
					premiereLigneTableau.append('Numéro')
					premiereLigneTableau.append('Nom')
					premiereLigneTableau.append('Prénom')
					premiereLigneTableau.append(ligne[3])
					
					for entete in ligne:
						
						# on ne prend en compte que les colonne à partir de la numéro 4
						if numCol > 3:
							premiereLigneTableau.append(entete)
							listeCodeEntete[numCol] = entete
							
						numCol = numCol + 1
					
					# flag pour savoir que l'on est dans le bon tableau
					dansLeTableauDesMoyennes = True
					
					# la ligne 0 correspond à la première ligne après l'entête du tableau, ce compteur permet de passer les premières ligne qui sont inutiles
					numLigneDansTableauDesMoyennes = 0
					
				else:
					dansLeTableauDesMoyennes = False
			
			# ligne de séparation des tableaux (c'est une ligne vide mais tout de même formaté)
			elif ligne[0] == '' and ligne[1] == '' and ligne[2] == '' and ligne[3] == '':
				
				# on sort du premier TableauDesMoyennes
				dansLeTableauDesMoyennes = False
			
			elif dansLeTableauDesMoyennes:
				
				# les 2 première lignes du tableau ne nous intéressent pas
				if numLigneDansTableauDesMoyennes > 1:
					
					# on récupère notre Etudiant et le modèle qui lui est associé
					try:
						etudiant = Etudiant.objects.get(numero=ligne[0])
						
						# il faudra calculer le résultat de cet étudiant
						listeEtudiantsACalculerResultat.append(etudiant)
						
						# on récupère notre ModeleSemestre qu'on importe. /!\ si on importe le S4, il peut y en avoir plusieur
						# ...
						
						# 1) on récupère  ! UNE ! Association_Etudiant_SemestreAnnualise entre notre étudiant et le semestre dont on importe les moyennes
						assoc = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualise)
						
						# 2) on récupère le ModeleSemestre liée à cette Association_Etudiant_SemestreAnnualise
						modeleSemestre = assoc.modeleSemestre
						
						# on récupère nos ModeleUE
						listeModeleUE = ModeleUE.objects.filter(modeleSemestre=modeleSemestre)
						
						# on récupère nos ModeleModule
						listeModeleModule = ModeleModule.objects.none()
						for ue in listeModeleUE:
							listeModeleModule = listeModeleModule | ModeleModule.objects.filter(modeleUE=ue)
						
						# on teste si les entêtes font bien parti du modèle de formation de l'étudiant
						
						print(listeCodeEntete)
						
						for key, entete in listeCodeEntete.items():
							try:
								
								listeModeleUE.get(codeApogee=entete)
							
							except ModeleUE.DoesNotExist:
								
								try:
									
									modeleModule = listeModeleModule.get(codeApogee=entete)
								
								except ModeleModule.DoesNotExist:
									
									titreErreur = 'Erreur d\'importation'
									messageErreur = 'L\'un des code d\'entête du fichier ne correspond à aucun semestre, UE ou module référencé par le modèle de formation de l\'étudiant ' + str(etudiant) + '.'
									detailsErreur = []
									
									detailsErreur.append('Le fichier comporte le code \'' + entete + '\' mais celui-ci n\'est pas référencé par le modèle de formation \'' + modeleSemestre.modeleFormation.nom + '\' pour le semestre \'' + modeleSemestre.intitule + '\'.')
									
									return render(request, 'erreurUtilisateur.html', locals())
						
						# on ajoute les info de l'étudiant à la ligne du tableau final à afficher
						ligneTableau = []
						ligneTableau.append(etudiant.numero)
						ligneTableau.append(etudiant.nom)
						ligneTableau.append(etudiant.prenom)
						
					# l'Etudiant n'existe pas !!!
					except Etudiant.DoesNotExist:
						titreErreur = 'Erreur d\'importation'
						messageErreur = 'L\'un des étudiants du fichier n\'existe pas dans la base de l\'application.'
						detailsErreur = []
						
						detailsErreur.append('Le fichier comporte l\'étudiant ' + ligne[1] + ' ' + ligne[2] + ' (numéro : ' + ligne[0] + ') mais il n\'existe pas dans la base de l\'application.')
						
						return render(request, 'erreurUtilisateur.html', locals())
						
					# On regarde si cet étudiant à déjà fait ce semestre (capitalisation d'UE)
					try:
						
						anneeUniversitaireActuel = semestreAnnualise.anneeUniversitaire.anneeUniversitaire
			
						annee1,annee2 = anneeUniversitaireActuel.split('/')
						annee1 = int(annee1) - 1
						annee2 = int(annee2) - 1
						anneeUniversitaireNmoins1String = str(annee1) + '/' + str(annee2)
						anneeUniversitaireNmoins1 = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1String)
						
						memeSemestreAnneeNmoins1 = SemestreAnnualise.objects.get(semestre=semestreAnnualise.semestre, anneeUniversitaire=anneeUniversitaireNmoins1)
						
						Association_Etudiant_SemestreAnnualise.objects.get(semestre=memeSemestreAnneeNmoins1, etudiant=etudiant)
						
						# cet étudiant à déjà fait ce semestre l'année dernière !
						redoublant = True
						
						moyennesModuleTmp = []
					
					except AnneeUniversitaire.DoesNotExist:
						
						redoublant = False
					
					except SemestreAnnualise.DoesNotExist:
						
						redoublant = False
						
					except Association_Etudiant_SemestreAnnualise.DoesNotExist:
						
						redoublant = False
					
					# on lui attribut ses moyennes
					numCol = 0
					
					for moyenne in ligne:
						
						# le semestre est toujours la 3éme colonne, # c'est la moyenne du semestre
						if numCol == 3:
							moyenne = moyenne.replace(',', '.')
							if(moyenne == ''):
								moyenne = '0'
							moyenne = float(moyenne)
							ligneTableau.append(moyenne)
							
							try:
								# si la moyenne existe déjà on la met à jour
								moyenneSemestre = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleSemestre=modeleSemestre)
								moyenneSemestre.moyenne = moyenne
								moyenneSemestre.save()
							
							except MoyenneSemestre.DoesNotExist:
								# sinon on la crée
								MoyenneSemestre(moyenne=moyenne, etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleSemestre=modeleSemestre).save()
						
						# le numéro, le nom et le prénom de sont pas des moyennes !!
						elif numCol > 3:
							moyenne = moyenne.replace(',', '.')
							if(moyenne == ''):
								moyenne = '0'
							moyenne = float(moyenne)
							ligneTableau.append(moyenne)
							
							# on cherche si c'est la moyenne du semestre, d'une UE ou d'un module
							try:
								modeleModule = listeModeleModule.get(codeApogee=listeCodeEntete[numCol])
								
								# c'est la moyenne d'un module
								try:
									# si la moyenne existe déjà on la met à jour
									moyenneModule = MoyenneModule.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleModule=modeleModule)
									moyenneModule.moyenne = moyenne
									moyenneModule.save()
								
								except MoyenneModule.DoesNotExist:
									# sinon on la crée
									MoyenneModule(moyenne=moyenne, etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleModule=modeleModule).save()
								
							except ModeleModule.DoesNotExist:
								
								modeleUE = listeModeleUE.get(codeApogee=listeCodeEntete[numCol])
								
								# c'est la moyenne d'une UE
								try:
									# si la moyenne existe déjà on la met à jour
									moyenneUE = MoyenneUE.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleUE=modeleUE)
									
									# si c'est un redoublant on regarde si il peut conserver sont ancienne moyenne
									if(redoublant):
										ancienneMoyenneUE = MoyenneUE.objects.get(etudiant=etudiant, semestreAnnualise=memeSemestreAnneeNmoins1, modeleUE=modeleUE).moyenne
										if(ancienneMoyenneUE > moyenne and ancienneMoyenneUE >= 10):
											moyenne = ancienneMoyenneUE
									
									moyenneUE.moyenne = moyenne
									moyenneUE.save()
								
								except MoyenneUE.DoesNotExist:
									# sinon on la crée
									
									# si c'est un redoublant on regarde si il peut conserver sont ancienne moyenne
									if(redoublant):
										ancienneMoyenneUE = MoyenneUE.objects.get(etudiant=etudiant, semestreAnnualise=memeSemestreAnneeNmoins1, modeleUE=modeleUE).moyenne
										if(ancienneMoyenneUE > moyenne and ancienneMoyenneUE >= 10):
											moyenne = ancienneMoyenneUE
											
									MoyenneUE(moyenne=moyenne, etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleUE=modeleUE).save()
							
						numCol = numCol + 1
					
					listeLigneTableau.append(ligneTableau)
					
					# si l'étudiant est redoublant on doit recalculer sa moyenne générale
					if(redoublant):
						
						moyennesUE = MoyenneUE.objects.filter(etudiant=etudiant, semestreAnnualise=semestreAnnualise)
						moyenneGenerale = 0.0
						sommeCoefGenerale = 0.0
						
						for moyenneUE in moyennesUE:
							
							moyenneGenerale = moyenneGenerale + (moyenneUE.moyenne * moyenneUE.modeleUE.coefficient)
							sommeCoefGenerale = sommeCoefGenerale + moyenneUE.modeleUE.coefficient
						
						moyenneGenerale = moyenneGenerale / sommeCoefGenerale
						
						moyenneSemestre = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualise, modeleSemestre=modeleSemestre)
						moyenneSemestre.moyenne = moyenneGenerale
						moyenneSemestre.save()
					
				numLigneDansTableauDesMoyennes = numLigneDansTableauDesMoyennes + 1
	
	calculerResultat(listeEtudiantsACalculerResultat, semestreAnnualise)
	
	semestreAnnualise.etatSemestre = EtatSemestrePossible.objects.get(etat='MOYENNES_IMPORTEES')
	semestreAnnualise.save()
	
	return render(request, 'etudiantManager/importerMoyenne.html', locals())

def getModeleFormation(etudiant):
	return etudiant.semestreActuel.modeleFormation

def calculerResultat(listeEtudiantsACalculerResultat, semestreAnnualise):
	print("calcule des résultat ...")
	
	# pour chaque étudiant dont on doit calculer le résultat du semestre (semestre N)
	for etudiant in listeEtudiantsACalculerResultat:
		
		assoc = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualise)
		
		# si ce n'est pas la première fois que l'on importe les moyennes, on réinitialise les résultats
		if(semestreAnnualise.etatSemestre.etat != 'EN_COURS'):
			assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
			assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='ATTENTE_DE_DECISION')
			assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='ATTENTE_DE_DECISION')
		
		# moyenne du semestre N (là c'est directement la valeur float de la moyenne)
		moySemestreN = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualise).moyenne
		
		# liste des MoyenneUE pour le semestre N
		listeMoyenneUEduSemestreN = MoyenneUE.objects.filter(etudiant=etudiant, semestreAnnualise=semestreAnnualise)
		
		# on regarde si on a pas de moyenne d'UE inférieur à 8
		aucuneMoyenneInferieurA8pourLeSemestreN = True
		for moyUE in listeMoyenneUEduSemestreN:
			if(moyUE.moyenne < 8):
				aucuneMoyenneInferieurA8pourLeSemestreN = False
		
		# cas de réorientation, commun à tout les semestres
		if(etudiant.statut.statut == 'réorienté' or etudiant.statut.statut == 'redoublant réorienté'):
			attribuerResultatAAssoc(assoc, 'DEF')
			
		# cas du semestre 1
		elif(semestreAnnualise.semestre.semestre == 'S1'):
			
			if(moySemestreN >= 10 and aucuneMoyenneInferieurA8pourLeSemestreN):
				attribuerResultatAAssoc(assoc, 'VAL')
			elif(aucuneMoyenneInferieurA8pourLeSemestreN):
				attribuerResultatAAssoc(assoc, 'NATT')
			else:
				attribuerResultatAAssoc(assoc, 'NATB')
        
		else:
			
			# cas du semestre 2
			if(semestreAnnualise.semestre.semestre == 'S2'):
				semestreAnnualiseS1 = SemestreAnnualise.objects.get(anneeUniversitaire=semestreAnnualise.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S1'))
				moySemestreNmoins1 = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualiseS1).moyenne
				resultatSemestreNmoins1 = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualiseS1 ).resultatSemestreJury.codeResultat
			
			# cas du semestre 3
			if(semestreAnnualise.semestre.semestre == 'S3'):
				
				#on récupère un semestre de l'année précédente ! il faut commencer par récupérer cette année précédente
				anneeUniversitaireActuel = semestreAnnualise.anneeUniversitaire.anneeUniversitaire
				annee1,annee2 = anneeUniversitaireActuel.split('/')
				annee1 = int(annee1) - 1
				annee2 = int(annee2) - 1
				anneeUniversitaireNmoins1String = str(annee1) + '/' + str(annee2)
				
				try:
					anneeUniversitaireNmoins1 = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1String)
					
				except AnneeUniversitaire.DoesNotExist:
					
					# erreur, pas de S2 et pourtant on veut calculer les résultat du S3
					titreErreur = 'Erreur lors du calcule de résultat'
					messageErreur = 'Les étudiants dont vous voulez importer les moyennes du S3 n\'ont pas de S2 !.'
					detailsErreur = []
					
					return render(request, 'erreurUtilisateur.html', locals())
							
				semestreAnnualiseS2 = SemestreAnnualise.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1, semestre=SemestrePossible.objects.get(semestre='S2'))
				moySemestreNmoins1 = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualiseS2).moyenne
				resultatSemestreNmoins1 = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualiseS2 ).resultatSemestreJury.codeResultat
			
			# cas du semestre 4
			if(semestreAnnualise.semestre.semestre == 'S4'):
				semestreAnnualiseS3 = SemestreAnnualise.objects.get(anneeUniversitaire=semestreAnnualise.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S3'))
				moySemestreNmoins1 = MoyenneSemestre.objects.get(etudiant=etudiant, semestreAnnualise=semestreAnnualiseS3).moyenne
				resultatSemestreNmoins1 = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualiseS3 ).resultatSemestreJury.codeResultat
			
			# le code qui suit est exécuté pour le S2, S3 et S4 ...
			
			if(aucuneMoyenneInferieurA8pourLeSemestreN):
				
				if(moySemestreN >= 10):
					
					if(resultatSemestreNmoins1 == 'VAL' or resultatSemestreNmoins1 == 'VALC' or resultatSemestreNmoins1 == 'VALJ'):
						
						attribuerResultatAAssoc(assoc, 'VAL')
						
					elif(moySemestreN + moySemestreNmoins1 >= 20 and resultatSemestreNmoins1 == 'NATT'):
						
						attribuerResultatAAssoc(assoc, 'ADAC')
						
					else:
						
						attribuerResultatAAssoc(assoc, 'AJPC')
						
				else:
				
					if((resultatSemestreNmoins1 == 'VAL' or resultatSemestreNmoins1 == 'VALC' or resultatSemestreNmoins1 == 'VALJ') and moySemestreN + moySemestreNmoins1 >= 20):
						
						attribuerResultatAAssoc(assoc, 'VALC')
			
					else:
						
						attribuerResultatAAssoc(assoc, 'NATT')
			else:
				
				attribuerResultatAAssoc(assoc, 'NATB')
			
		assoc.save()

def attribuerResultatAAssoc(assoc, resultat):
	
	typeSemestre = assoc.semestre.semestre.semestre
	etudiant = assoc.etudiant
	
	if(typeSemestre == 'S2'):
		
		semestreAnnualiseNmoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=assoc.semestre.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S1'))
		devenirSemestreSuivant = DevenirEtudiantPossible.objects.get(code='S3')
		devenirSemestrePrecedent = DevenirEtudiantPossible.objects.get(code='S1')
	
	if(typeSemestre == 'S3'):
		
		#on récupère un semestre de l'année précédente ! il faut commencer par récupérer cette année précédente
		anneeUniversitaireActuel = assoc.semestre.anneeUniversitaire.anneeUniversitaire
		annee1,annee2 = anneeUniversitaireActuel.split('/')
		annee1 = int(annee1) - 1
		annee2 = int(annee2) - 1
		anneeUniversitaireNmoins1String = str(annee1) + '/' + str(annee2)
		
		try:
			anneeUniversitaireNmoins1 = AnneeUniversitaire.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1String)
			
		except AnneeUniversitaire.DoesNotExist:
			
			# erreur, pas de S2 et pourtant on veut calculer les résultat du S3
			titreErreur = 'Erreur lors du calcule de résultat'
			messageErreur = 'Les étudiants dont vous voulez importer les moyennes du S3 n\'ont pas de S2 !.'
			detailsErreur = []
			
			return render(request, 'erreurUtilisateur.html', locals())
		
		semestreAnnualiseNmoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=anneeUniversitaireNmoins1, semestre=SemestrePossible.objects.get(semestre='S2'))
		devenirSemestreSuivant = DevenirEtudiantPossible.objects.get(code='S4')
		devenirSemestrePrecedent = DevenirEtudiantPossible.objects.get(code='S2')
	
	if(typeSemestre == 'S4'):
		
		semestreAnnualiseNmoins1 = SemestreAnnualise.objects.get(anneeUniversitaire=assoc.semestre.anneeUniversitaire, semestre=SemestrePossible.objects.get(semestre='S3'))
	
	if(typeSemestre != 'S1'):
		assocNmoins1 = Association_Etudiant_SemestreAnnualise.objects.get(etudiant=etudiant, semestre=semestreAnnualiseNmoins1)
		resultatNmoins1 = assocNmoins1.resultatSemestreJury.codeResultat
	
	# on attribut le résultat calculé
	assoc.resultatSemestreCalcule = ResultatSemestrePossible.objects.get(codeResultat=resultat)
	
	# on cherche à attribuer les résultats pré-jury et jury et le devenir par défaut
	# si plusieur résultats Jury sont possible pour un même résultat calculé, on laisse le résultat et le devenir ATTENTE_DE_DECISION
	
	# cas du semestre 1
	if(typeSemestre == 'S1'):
		assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
		assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
		
		if(resultat != 'DEF'):
			assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='S2')
	
	# cas du semestre 2 et 3
	if(typeSemestre == 'S2' or typeSemestre == 'S3'):
		if(resultatNmoins1 == 'VAL'):
			
			if(resultat != 'DEF'):
				assoc.devenirEtudiant = devenirSemestreSuivant
			
			assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
			assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
			
		if(resultatNmoins1 == 'NATT'):
			
			if(resultat == 'ADAC'):
				assoc.devenirEtudiant = devenirSemestreSuivant
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='ADAC')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='ADAC')
				
			elif(resultat == 'DEF'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
		
		if(resultatNmoins1 == 'NATB'):
			
			if(resultat == 'DEF'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
		
		# uniquement possible si on fait le jury S3, les résultats VALC et ADAC ne peuvant pas être attribué au S1
		if(resultatNmoins1 == 'VALC' or resultatNmoins1 == 'ADAC'):
			
			assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
			assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
			
			if(resultat != 'DEF'):
				assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='S4')
				
		if(resultatNmoins1 == 'DEF'):
			
			if(resultat == 'DEF'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
				
			else:
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='NVAL')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='NVAL')
			
			if(resultat == 'AJPC'):
				assoc.devenirEtudiant = devenirSemestrePrecedent
	
	# cas du semestre 4
	if(typeSemestre == 'S4'):
		if(resultatNmoins1 == 'VAL'):
			
			if(resultat == 'VAL' or resultat == 'VALC'):
				assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='DUT')
			
			if(resultat != 'NATT' and resultat != 'NATB'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
		
		if(resultatNmoins1 == 'NATT'):
			
			if(resultat == 'ADAC'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='ADAC')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='ADAC')
				assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='DUT')
			
			if(resultat == 'DEF'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
		
		if(resultatNmoins1 == 'NATB'):
			
			if(resultat == 'DEF'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='DEF')
		
		if(resultatNmoins1 == 'VALC' or resultatNmoins1 == 'ADAC'):
			
			if(resultat == 'VAL' or resultat == 'DEF'):
				assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
				assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat=resultat)
			
			if(resultat == 'VAL'):
				assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='DUT')
		
		if(resultatNmoins1 == 'DEF'):
			
			assoc.resultatSemestrePreJury = ResultatSemestrePossible.objects.get(codeResultat='NVAL')
			assoc.resultatSemestreJury = ResultatSemestrePossible.objects.get(codeResultat='NVAL')
			
			if(resultat == 'AJPC'):
				assoc.devenirEtudiant = DevenirEtudiantPossible.objects.get(code='S3')
	
class TableauMoyenne():
	semestreAnnualise = None
	ligneEntete = ''
	moyenneSemestre = 0
	listeDivisionUE = []
	nbUE = 0

class DivisionUE():
	numeroUE = 0
	labelUE = ''
	moyenneUE = 0
	listeDivisionModule = []
	nbModule = 0

class DivisionModule():
	numeroModule = 0
	labelModule = ''
	moyenneModule = 0
	
class KeyValue():
	key = None
	value = None
