# coding: utf8
from django import forms
from home.models import Etudiant, ModeleModule, ModeleUE, ModeleSemestre, ModeleFormation, BAC, CursusPossible, CursusPreDUT, CursusPostDUT, AnneeUniversitaire, Association_Etudiant_SemestreAnnualise

class DateInput(forms.DateInput):
    input_type = 'date'

class EtudiantForm(forms.ModelForm):
	class Meta:
		model = Etudiant
		exclude = ('numero', 'statut', 'semestreActuel', 'BAC', 'mentionBAC', 'anneeBAC',)
		widgets = {
            'dateNaissance': DateInput(),
        }

class EtudiantFormAll(forms.ModelForm):
	class Meta:
		model = Etudiant
		fields = '__all__'

class ModuleForm(forms.ModelForm):
	class Meta:
		model = ModeleModule
		exclude = ('modeleUE',)

class UEForm(forms.ModelForm):
	class Meta:
		model = ModeleUE
		exclude = ('modeleSemestre',)

class SemestreForm(forms.ModelForm):
	class Meta:
		model = ModeleSemestre
		exclude = ('modeleFormation','typeSemestre',)

class ModeleForm(forms.ModelForm):
	class Meta:
		model = ModeleFormation
		fields = '__all__'

class BACForm(forms.ModelForm):
	class Meta:
		model = BAC
		fields = '__all__'
		
class CursusForm(forms.ModelForm):
	class Meta:
		model = CursusPossible
		fields = '__all__'

class CursusPreDUTForm(forms.ModelForm):
	class Meta:
		model = CursusPreDUT
		exclude = ('etudiant', 'anneeUniversitaire',)

class CursusPostDUTForm(forms.ModelForm):
	class Meta:
		model = CursusPostDUT
		exclude = ('etudiant', 'anneeUniversitaire',)

class Association_Etudiant_SemestreAnnualiseForm(forms.ModelForm):
	class Meta:
		model = Association_Etudiant_SemestreAnnualise
		exclude = ('etudiant', 'semestre', 'modeleSemestre', 'resultatSemestreCalcule', 'devenirEtudiant')

class CalculerStatistiquesForm(forms.ModelForm):
	class Meta:
		model = AnneeUniversitaire
		exclude = ('anneeUniversitaire','semestre',)
