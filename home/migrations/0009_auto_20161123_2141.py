# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-23 20:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0008_auto_20161122_1026'),
    ]

    operations = [
        migrations.CreateModel(
            name='CursusPossible',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=100)),
                ('informationsSupplementaires', models.TextField(blank=True, max_length=500, null=True, verbose_name='Informations supplémentaires')),
            ],
        ),
        migrations.CreateModel(
            name='CursusPostDUT',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lieu', models.CharField(max_length=100)),
                ('informationsSupplementaires', models.CharField(blank=True, max_length=500, null=True, verbose_name='Informations Supplémentaires')),
                ('anneeUniversitaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.AnneeUniversitaire')),
                ('cursus', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.CursusPossible')),
                ('etudiant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.Etudiant')),
            ],
        ),
        migrations.CreateModel(
            name='CursusPreDUT',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lieu', models.CharField(max_length=100)),
                ('informationsSupplementaires', models.CharField(blank=True, max_length=500, null=True, verbose_name='Informations supplémentaires')),
                ('anneeUniversitaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.AnneeUniversitaire')),
                ('cursus', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.CursusPossible')),
                ('etudiant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.Etudiant')),
            ],
        ),
        migrations.CreateModel(
            name='TypeCursus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('intitule', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='cursuspossible',
            name='typeCursus',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.TypeCursus', verbose_name='Type'),
        ),
    ]
