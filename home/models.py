# coding: utf8
from django.db import models

# Create your models here.

"""
Un etudiant et toutes ses infos
numero : unique, propre à chaque étudiant, OBLIGATOIRE
nom, prenom : OBLIGATOIRE
statut : permet de différencier les étudiants "normaux", redoublants ou réorientés
semestreActuel : son semestre actuel (EX: 'S1 2016/2017'), OBLIGATOIRE
sexe, telephone, dateNaissance, adresse, email, informationsSupplementaires : Info variés optionelles
BAC : quel BAC à fait cet étudiant, optionel
mentionBAC : La mention qu'il a obtenu, optionel
anneeBAC : l'AnneeUniversitaire durant laquelle l'étudiant a obtenu son BAC
"""
class Etudiant(models.Model):
	numero = models.IntegerField(verbose_name='Numéro étudiant')
	nom = models.CharField(max_length=50)
	prenom = models.CharField(max_length=50)
	statut = models.ForeignKey('StatutEtudiant')
	
	# permet de savoir dans quel semestre est inscrit l'étudiant
	semestreActuel = models.ForeignKey('SemestreAnnualise', verbose_name='Semestre actuel', null=True, blank=True)
	
	sexe = models.ForeignKey('Sexe', null=True, blank=True)
	telephone = models.CharField(max_length=50, null=True, blank=True)
	dateNaissance = models.DateField(null=True, blank=True, verbose_name='Date de naissance')
	adresse = models.CharField(max_length=200, null=True, blank=True)
	email = models.CharField(max_length=100, null=True, blank=True)
	informationsSupplementaires = models.TextField(max_length=500, null=True, blank=True, verbose_name='Informations supplémentaires')
	
	# informations concernant le BAC
	BAC = models.ForeignKey('BAC', verbose_name='BAC', null=True, blank=True)
	mentionBAC = models.ForeignKey('MentionBACPossible', verbose_name='Mention du BAC', null=True, blank=True)
	anneeBAC = models.ForeignKey('AnneeUniversitaire', verbose_name='Année du BAC', null=True, blank=True)
	
	def __str__(self):
		return str(self.numero) + " : " + self.prenom + " " + self.nom

"""
La liste exaustive et fini des sexes : M et F
"""		
class Sexe(models.Model):
	sexe = models.CharField(max_length=1)
	
	def __str__(self):
		return self.sexe


"""
La liste exaustive et fini des statut possible d'un étudiant (normal, redoublant, réorienté, diplomé)
"""
class StatutEtudiant(models.Model):
	statut = models.CharField(max_length=50)
	
	def __str__(self):
		return self.statut

"""
La liste exaustive et fini des semestre d'un DUT : S1, S2, S3, S4
"""
class SemestrePossible(models.Model):
	semestre = models.CharField(max_length=2)
	
	def __str__(self):
		return self.semestre

"""
Les années universitaire : EX: '2016/2017', '2013/2014'
"""
class AnneeUniversitaire(models.Model):
	anneeUniversitaire = models.CharField(max_length=20)
	
	def __str__(self):
		return str(self.anneeUniversitaire)

"""
Les semestres de chaque année et le modèle de formation qui lui est lié : Ex: 'S1 2016/2017'
"""
class SemestreAnnualise(models.Model):
	semestre = models.ForeignKey('SemestrePossible')
	anneeUniversitaire = models.ForeignKey('AnneeUniversitaire')
	modeleFormation = models.ForeignKey('ModeleFormation')
	etatSemestre = models.ForeignKey('EtatSemestrePossible')
	
	def __str__(self):
		return self.semestre.__str__() + " " + self.anneeUniversitaire.__str__()

"""

"""
class EtatSemestrePossible(models.Model):
	etat = models.CharField(max_length=50)
	
	def __str__(self):
		return self.etat

"""
Permet de relier un étudiant à l'ensemble des semestre qu'il a fait
Contrairement à semestreActuel de Etudiant, les ligne de cette table perdurent dans le temps
"""
class Association_Etudiant_SemestreAnnualise(models.Model):
	etudiant = models.ForeignKey('Etudiant')
	semestre = models.ForeignKey('SemestreAnnualise')
	modeleSemestre = models.ForeignKey('ModeleSemestre')
	resultatSemestreCalcule = models.ForeignKey('ResultatSemestrePossible', related_name='resultatSemestreCalcule')
	resultatSemestrePreJury = models.ForeignKey('ResultatSemestrePossible', related_name='resultatSemestrePreJury')
	resultatSemestreJury = models.ForeignKey('ResultatSemestrePossible', related_name='resultatSemestreJury')
	devenirEtudiant = models.ForeignKey('DevenirEtudiantPossible')
	
	def __str__(self):
		return self.etudiant.__str__() + " " + self.semestre.__str__()

"""
Liste exaustive et fini des codes de resultat possible pour un semestre plus l'état 'ATTENTE_DE_DECISION' pour signaler que le résultat n'est pas encore calculé:
	ATTENTE_DE_DECISION, VAL, NATT, NATB, DEF, ADAC, AJPC, VALC
"""
class ResultatSemestrePossible(models.Model):
	codeResultat = models.CharField(max_length=20)
	
	def __str__(self):
		return self.codeResultat

"""
Liste exaustive et fini des codes pour le devenir d'un étudiant plus l'état 'ATTENTE_DE_DECISION' pour signaler que le résultat n'est pas encore calculé:
	ATTENTE_DE_DECISION, Réo, S1, S2, S3, S4, DUT
"""
class DevenirEtudiantPossible(models.Model):
	code = models.CharField(max_length=20)
	
	def __str__(self):
		return self.code
	
"""
Liste des semestre dont on a généré le doc jury mais pas encore cloturés
"""
class SemestreAnnualiseJuryEnCours(models.Model):
	semestreAnnualise = models.ForeignKey('SemestreAnnualise')
	
	def __str__(self):
		return self.semestreAnnualise

"""
Le modèle qui représente la structure de la formation au sein du DUT
nom : unique, permet d'identifier le modèle sur l'interface utilisateur, OBLIGATOIRE
description : optionel
"""
class ModeleFormation(models.Model):
	nom = models.CharField(max_length=100)
	description = models.TextField(max_length=500, null=True, blank=True)
	
	def __str__(self):
		return self.nom

"""
Modèle d'un semestre du DUT
typeSemestre : S1, S2, S3 ou S4, OBLIGATOIRE
intitule : unique, permet d'identifier le modèle sur l'interface utilisateur, OBLIGATOIRE
modeleFormation : le modèle de formation qui comprend ce modèle de semestre, OBLIGATOIRE
"""
class ModeleSemestre(models.Model):
	codeApogee = models.CharField(max_length=100)
	typeSemestre = models.ForeignKey('SemestrePossible')
	intitule = models.CharField(max_length=100)
	modeleFormation = models.ForeignKey('ModeleFormation')
	
	def __str__(self):
		return self.intitule

"""
Modèle d'une UE du DUT
code : le code de l'UE, permet d'importer les moyennes! OBLIGATOIRE
intitule : unique, permet d'identifier le modèle sur l'interface utilisateur, OBLIGATOIRE
coefficient : le coefficient de l'UE
modeleSemestre : le modèle de semestre qui comprend ce modèle d'UE, OBLIGATOIRE
"""
class ModeleUE(models.Model):
	codeApogee = models.CharField(max_length=100)
	code = models.CharField(max_length=100)
	intitule = models.CharField(max_length=100)
	coefficient = models.FloatField()
	modeleSemestre = models.ForeignKey('ModeleSemestre')
	
	def __str__(self):
		return self.intitule

"""
Modèle d'un module du DUT
code : le code du module, permet d'importer les moyennes! OBLIGATOIRE
intitule : unique, permet d'identifier le modèle sur l'interface utilisateur, OBLIGATOIRE
coefficient : le coefficient du module
modeleUE : le modèle d'UE qui comprend ce modèle de module, OBLIGATOIRE
"""
class ModeleModule(models.Model):
	codeApogee = models.CharField(max_length=100)
	code = models.CharField(max_length=100)
	intitule = models.CharField(max_length=100)
	coefficient = models.FloatField()
	modeleUE = models.ForeignKey('ModeleUE')
	
	def __str__(self):
		return self.intitule

"""
La moyenne d'un Etudiant donné pour un ModeleSemestre donné
semestreAnnualise : quand l'Etudiant a t'il obtenu cet moyenne, util pour le cas de redoublement de semestre
"""
class MoyenneSemestre(models.Model):
	moyenne = models.FloatField()
	modeleSemestre = models.ForeignKey('ModeleSemestre')
	etudiant = models.ForeignKey('Etudiant')
	semestreAnnualise = models.ForeignKey('SemestreAnnualise')
	
	def __str__(self):
		return self.semestreAnnualise.__str__() + " " + self.modeleSemestre.__str__() + " " + self.etudiant.__str__() + ' ' + str(self.moyenne)

"""
La moyenne d'un Etudiant donné pour un ModeleUE donné
semestreAnnualise : quand l'Etudiant a t'il obtenu cet moyenne, util pour le cas de redoublement de semestre
"""
class MoyenneUE(models.Model):
	moyenne = models.FloatField()
	modeleUE = models.ForeignKey('ModeleUE')
	etudiant = models.ForeignKey('Etudiant')
	semestreAnnualise = models.ForeignKey('SemestreAnnualise')
	
	def __str__(self):
		return self.semestreAnnualise.__str__() + " " + self.modeleUE.__str__() + " " + self.etudiant.__str__() + ' ' + str(self.moyenne)

"""
La moyenne d'un Etudiant donné pour un ModeleModule donné
semestreAnnualise : quand l'Etudiant a t'il obtenu cet moyenne, util pour le cas de redoublement de semestre
"""
class MoyenneModule(models.Model):
	moyenne = models.FloatField()
	modeleModule = models.ForeignKey('ModeleModule')
	etudiant = models.ForeignKey('Etudiant')
	semestreAnnualise = models.ForeignKey('SemestreAnnualise')
	
	def __str__(self):
		return self.semestreAnnualise.__str__() + " " + self.modeleModule.__str__() + " " + self.etudiant.__str__() + ' ' + str(self.moyenne)

"""
La liste exaustive et fini des type de BAC existant
	Général, Technologique, Professionnel, Autre
"""
class TypeBAC(models.Model):
	typeBAC = models.CharField(max_length=50)

	def __str__(self):
		return self.typeBAC

"""
Liste des BAC existant, modifiable par l'utilisateur
intitule : unique, permet d'identifier le modèle sur l'interface utilisateur, OBLIGATOIRE
typeBAC : de quel type est ce BAC (Général, Technologique, Professionnel ou Autre)
informationsSupplementaires : Info variés optionelles
"""
class BAC(models.Model):
	intitule = models.CharField(max_length=100)
	typeBAC = models.ForeignKey('TypeBAC', verbose_name='Type de BAC')
	informationsSupplementaires = models.TextField(max_length=400, null=True, blank=True, verbose_name='Informations supplémentaires')

	def __str__(self):
		return self.intitule + " (" + self.typeBAC.__str__() + ")"

"""
La liste exaustive et fini des mentions possible au BAC plus 'Non' si pas de mention
	'Non', 'Assez Bien', 'Bien', 'Très Bien'
"""
class MentionBACPossible (models.Model):
	mentionBAC = models.CharField(max_length=20, null=True, blank=True)
	
	def __str__(self):
		return self.mentionBAC

"""
Un TypeCursus définie le type de Cursus possible
"""
class TypeCursus(models.Model):
	intitule = models.CharField(max_length=100)
	
	def __str__(self):
		return str(self.intitule)
		
"""
Un CursusPossible est composé d'un intitulé (obligatoire)
et de plusieur autre champ qui peut être vide.
Il est rattaché à un TypeCursus.
"""
class CursusPossible(models.Model):
	intitule = models.CharField(max_length=100)
	typeCursus = models.ForeignKey('TypeCursus', verbose_name='Type')
	ville = models.CharField(max_length=100, null=True, blank=True)
	codePostal = models.CharField(max_length=100, null=True, blank=True)
	adresse1 = models.CharField(max_length=200, null=True, blank=True)
	adresse2 = models.CharField(max_length=200, null=True, blank=True)
	nomEcoleEntreprise = models.CharField(max_length=200, null=True, blank=True)
	specialite = models.CharField(max_length=200, null=True, blank=True)
	option = models.CharField(max_length=200, null=True, blank=True)
	domaineActivite = models.CharField(max_length=300, null=True, blank=True)
	posteOccupe = models.CharField(max_length=200, null=True, blank=True)
	informationsSupplementaires = models.TextField(max_length=500, null=True, blank=True, verbose_name='Informations supplémentaires')
	
	def __str__(self):
		return str(self.intitule) + " " + self.informationsSupplementaires + " " + self.typeCursus.intitule
		
"""
Un CursusPreDUT est un cursus qu'un étudiant
peut avoir suivie avant d'entrer en DUT.
	-cursus : Le cursus suivie.
	-etudiant : L'étudiant ayant suivie ce cursus avant le DUT.
	-anneeUniversitaire : L'année universitaire durant laquelle il a effectué ce cursus.
	-lieu : Le lieu où l'étudiant à suivie ce cursus.
	-informationsSupplementaires : Champs supplémentaire pour rajouter quelconques informations.
"""
class CursusPreDUT(models.Model):
	cursus = models.ForeignKey('CursusPossible')
	etudiant = models.ForeignKey('Etudiant')
	anneeUniversitaire = models.ForeignKey('AnneeUniversitaire')
	lieu = models.CharField(max_length=100, null=True, blank=True)
	informationsSupplementaires = models.TextField(max_length=500, null=True, blank=True, verbose_name='Informations supplémentaires')
	
	def __str__(self):
		return str(self.cursus) + " " + str(self.etudiant) + " " + str(self.anneeUniversitaire) + " " + self.lieu + " " + self.informationsSupplementaires

"""
Un CursusPostDUT est un cursus qu'un étudiant
peut suivie après son DUT.
	-cursus : Le cursus suivie.
	-etudiant : L'étudiant ayant suivie ce cursus avant le DUT.
	-anneeUniversitaire : L'année universitaire durant laquelle il a effectué ce cursus.
	-lieu : Le lieu où l'étudiant à suivie ce cursus.
	-informationsSupplementaires : Champs supplémentaire pour rajouter quelconques informations.
"""
class CursusPostDUT(models.Model):
	cursus = models.ForeignKey('CursusPossible')
	etudiant = models.ForeignKey('Etudiant')
	anneeUniversitaire = models.ForeignKey('AnneeUniversitaire')
	lieu = models.CharField(max_length=100, null=True, blank=True)
	informationsSupplementaires = models.TextField(max_length=500, null=True, blank=True, verbose_name='Informations supplémentaires')
	
	def __str__(self):
		return str(self.cursus) + " " + str(self.etudiant) + " " + str(self.anneeUniversitaire) + " " + self.lieu + " " + self.informationsSupplementaires

"""
La liste des directeurs de l'IUT
actif permet de savoir si c'est le directeur actuel
"""
class Directeur(models.Model):
	nomComplet = models.CharField(max_length=100, verbose_name='Nom complet')
	actif = models.BooleanField()
	
	def __str__(self):
		
		if(self.actif):
			stringActif = 'actif'
		else:
			stringActif = 'inactif'
		
		return self.nomComplet + " " + stringActif
