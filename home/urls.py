from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'travaux$', views.travaux),
    url(r'accueil$', views.home),
    url(r'$', views.home),
]
