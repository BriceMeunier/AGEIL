from django.apps import AppConfig


class JurymanagerConfig(AppConfig):
    name = 'juryManager'
