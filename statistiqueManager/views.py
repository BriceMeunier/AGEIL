#!/usr/bin/python2
# -*- coding: utf-8 -*-

from django.shortcuts import render
from home.models import Etudiant, SemestrePossible, Association_Etudiant_SemestreAnnualise, MoyenneSemestre, SemestreAnnualise, AnneeUniversitaire, Sexe, BAC, TypeBAC, MentionBACPossible, ResultatSemestrePossible
from home.forms import CalculerStatistiquesForm

def homeNombreEtudiant(request):
	return homeMessageNombreEtudiant(request, False)

def homeMessageNombreEtudiant(request, messageAAfficher):
	#pour donner les valeurs de l'année et le semestre dans le formulaire de page html

	AnneesUniversitaires = AnneeUniversitaire.objects.all()
	SemestrePossibles = SemestrePossible.objects.all()
	existeAnnee = 1
	#Si l'anneeuniversitare ou semestre ne sont pas encore choisi, On va pas afficher le formulaire
	if not AnneesUniversitaires :
		existeAnnee = 0

	nombreEtudiant = 0
	nombreFille = 0
	nombreSexeNull = 0
	nombreBacGeneral = 0
	nombreBacTechnologie = 0
	nombreBacProfessionnel = 0
	nombreBacAutre = 0
	nombreBacNull = 0
	nombreResultatValide = 0
	nombreRsultatNull = 0

	if request.method == 'POST':
		form = CalculerStatistiquesForm(request.POST)
		if form.is_valid():
			# on prend la valeur d'année universitaire et semestre qu'on a saisi
			anneeUniversitaire = AnneeUniversitaire.objects.get(anneeUniversitaire=request.POST['annee'])
			semestre = SemestrePossible.objects.get(semestre=request.POST['semestre'])
			#on cherche la liste d'étudiant qui correspondent aux anneé et semestre saisis
			semestreAnnualise = SemestreAnnualise.objects.filter(anneeUniversitaire=anneeUniversitaire).filter(semestre=semestre)
			listeEtudiantDemande = Association_Etudiant_SemestreAnnualise.objects.filter(semestre = semestreAnnualise)

			for assoc in listeEtudiantDemande:
				#on compte l'ensemble de nombre d'étudiant
				nombreEtudiant = nombreEtudiant + 1

				if(assoc.etudiant.sexe == None):
					nombreSexeNull = nombreSexeNull + 1
				else:
					sexEtudiant = assoc.etudiant.sexe.sexe

					if sexEtudiant == 'F':
						#on compte l'ensemble de nombre de fille
						nombreFille = nombreFille + 1

				#si jamais le bac n'est pas attribué à l'étudiant, on compte le nombre des étudiant qui n'ont pas le bac
				if(assoc.etudiant.BAC == None):
					nombreBacNull =	nombreBacNull + 1
				else:
					#sinon on compte le nombre de bac général, bac technologique, bac professionnel
					bacEtudiant =  assoc.etudiant.BAC.typeBAC.typeBAC
					if bacEtudiant == 'Général':
						nombreBacGeneral = nombreBacGeneral + 1
					elif bacEtudiant == 'Technologique':
						nombreBacTechnologie = nombreBacTechnologie + 1
					elif bacEtudiant == 'Professionnel':
						nombreBacProfessionnel = nombreBacProfessionnel + 1
					else:
						nombreBacAutre = nombreBacAutre + 1

				#premier semestre on a pas de résultat jury, du coup ça sera le résultatcalcul

				if (assoc.resultatSemestreJury.codeResultat == None):
					nombreRsultatNull = nombreRsultatNull + 1
				else:
					if (assoc.resultatSemestreJury.codeResultat == 'VAL' or assoc.resultatSemestreJury.codeResultat == 'VALC'):
						nombreResultatValide = nombreResultatValide+1 ;

				#s2,s3,s4 ca dependra le resultats de jury


			# on calcule le pourcent de nbre fille, nbre bac général, nbre bac technologique, nbre professionnel...par rapport à le nombre ensemble des étudiants
			# si jamais le nombre d'étudiant est 0, on peut pas calculer le pourcentage en divisant 0
			if nombreEtudiant == 0:
				FillePourcent = 0
				BacGeneralPourcent = 0
				BacTechnologiePourcent  = 0
				BacProfessionnelPourcent = 0
				BacAutrePourcent = 0
				BacNullPourcent = 0
				TauxReussiDeCeSemestre = 0
			#sinon on les calcule
			else:
				FillePourcent = round((nombreFille) / (nombreEtudiant)*100,2)
				BacGeneralPourcent = round((nombreBacGeneral) / (nombreEtudiant)*100,2)
				BacTechnologiePourcent  = round((nombreBacTechnologie) / (nombreEtudiant)*100,2)
				BacProfessionnelPourcent = round((nombreBacProfessionnel) / (nombreEtudiant)*100,2)
				BacAutrePourcent = round((nombreBacAutre) / (nombreEtudiant)*100,)
				# le pourcentage de bac n'a pas été attribué, mais on les affiche pas
				BacNullPourcent = round((nombreBacNull) / (nombreEtudiant)*100,2)
				TauxReussiDeCeSemestre = round((nombreResultatValide) / (nombreEtudiant)*100,2)

	return render(request, 'statistiqueManager/calculerStatistiquesDePromoN.html',locals())
